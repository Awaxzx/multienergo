docker-build:
	sudo chown awax:awax docker/mysql/*.pem && docker-compose up --build -d

docker-up:
	docker-compose up -d

docker-down:
	docker-compose down

docker-migrate:
	docker-compose exec php php artisan migrate:refresh