@extends('admin.layouts.admin')


@section('content')

    <section class="content-header">
    <h1>
        Dashboard

    </h1>

    @section('breadcrumbs', '')
    @yield('breadcrumbs')

</section>

<!-- Main content -->
<section class="content container-fluid">

    <!--------------------------
      | Your Page Content Here |
      -------------------------->

</section>

@endsection
