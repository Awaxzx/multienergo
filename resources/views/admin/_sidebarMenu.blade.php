<!-- Sidebar Menu -->
<ul class="sidebar-menu" data-widget="tree">
    <!-- Optionally, you can add icons to the links -->
    <li class="{{ request()->is('admin') ? ' active' : '' }}"><a href="{{ route('admin.dashboard') }}"><i class="fa fa-dashboard"></i><span>{{__('admin.MENU_DASHBOARD')}}</span></a></li>
    @can ('admin-panel')
        <li class="{{ request()->is('admin/page*') ? ' active' : '' }}"><a href="{{ route('admin.page') }}"><i class="fa fa-files-o"></i><span>{{__('admin.MENU_PAGES')}}</span></a></li>
    @endcan
    @can ('admin-panel')
        <li class="{{ request()->is('admin/category*') ? ' active' : '' }}"><a href="{{ route('admin.category') }}"><i class="fa fa-list"></i><span>{{__('admin.MENU_CATEGORIES')}}</span></a></li>
    @endcan
    @can ('admin-panel')
        <li class="{{ request()->is('admin/product*') ? ' active' : '' }}"><a href="{{ route('admin.product') }}"><i class="fa fa-shopping-cart"></i><span>{{__('admin.MENU_PRODUCTS')}}</span></a></li>
    @endcan
    @can ('admin-panel')
        <li class="{{ request()->is('admin/certificate*') ? ' active' : '' }}"><a href="{{ route('admin.certificate') }}"><i class="fa fa-certificate"></i><span>{{__('admin.MENU_CERTIFICATES')}}</span></a></li>
    @endcan
    @can ('admin-panel')
        <li class="{{ request()->is('admin/user*') ? ' active' : '' }}"><a href="{{ route('admin.user') }}"><i class="fa fa-users"></i><span>{{__('admin.MENU_USERS')}}</span></a></li>
    @endcan
</ul>
<!-- /.sidebar-menu -->
