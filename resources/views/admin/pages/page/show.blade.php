@extends('admin.layouts.onecolumn')

@section('header', __('admin.pages.TTL_VIEW_PAGE'))
@section('breadcrumbs', Breadcrumbs::render())
@section('main_block_content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="text-right">
                        <a href="{{route('admin.page')}}" class="btn btn-info"><span
                                    class="fa fa-list"></span> {{__('admin.TTL_BACK_TO_LIST')}}</a>
                        <a href="{{route('admin.page.edit', $page->id)}}" class="btn btn-success"><span
                                    class="fa fa-edit"></span> {{__('admin.TTL_EDIT')}}</a>
                        <form method="POST" action="{{ route('admin.page.destroy', $page) }}" class="inline">
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-danger" onclick="return confirm('{{__('admin.TTL_DELETE_SURE')}}')"><span
                                        class="fa fa-remove"></span> {{__('admin.TTL_DELETE')}}
                            </button>
                        </form>
                        <a href="{{route('admin.page.create')}}" class="btn btn-primary"><span
                                    class="fa fa-plus"></span> {{__('admin.pages.TTL_ADD_NEW_PAGE')}}</a>
                    </div>
                    <!-- Main content -->
                    <dl class="dl-horizontal">
                        <dt>ID</dt>
                        <dd>{{$page->id}}</dd>

                        <dt>{{__('admin.pages.ATR_NAME')}}</dt>
                        <dd>{{$page->name}}</dd>

                        <dt>{{__('admin.pages.ATR_URI')}}</dt>
                        <dd>{{$page->uri}}</dd>

                        <dt>{{__('admin.pages.ATR_CONTENT')}}</dt>
                        <dd>{{$page->content}}</dd>

                        <dt>{{__('admin.ATR_SORT')}}</dt>
                        <dd>{{$page->sort}}</dd>

                        <dt>{{__('admin.ATR_STATUS')}}</dt>
                        <dd>{{$page->status}}</dd>
                    </dl>

                    @include('admin.meta._show_meta', ['item' => $page])
                    @include('admin.files._show_files', ['files' => $page->getFiles()])

                </div>
            </div>
        </div>
    </div>
@endsection
