@extends('admin.layouts.onecolumn')

@section('header', __('admin.pages.TTL_EDIT_PAGE'))
@section('breadcrumbs', Breadcrumbs::render())
@section('main_block_content')
    <!-- Main content -->

    <div class="row">
        <div class="col-xs-12">
            <div class="box">

                @include('admin.pages.page._form', [
                'method' => 'put',
                'mode' => 'update',
                'page' => $page,
                'tree' => $tree,
                'menus' => $menus,
                'action' => route('admin.page.update', ['page' => $page])
                ])

            </div>
        </div>
    </div>

@endsection
