@extends('admin.layouts.onecolumn')

@section('header', __('admin.pages.TTL_PAGES'))
@section('breadcrumbs', Breadcrumbs::render())
@section('main_block_content')

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <div class="text-right"><a href="{{route('admin.page.create')}}" class="btn btn-primary"><span
                                class="fa fa-plus"></span> {{__('admin.pages.TTL_ADD_NEW_PAGE')}}</a></div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="table1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>{{__('admin.pages.ATR_NAME')}}</th>
                            <th>{{__('admin.pages.ATR_URI')}}</th>
                            <th>{{__('admin.TTL_SORT')}}</th>
                            <th>{{__('admin.TTL_STATUS')}}</th>
                            <th>{{__('admin.TTL_CREATED_AT')}}</th>
                            <th>{{__('admin.TTL_UPDATED_AT')}}</th>
                            <th>{{__('admin.TTL_ACTIONS')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($pages as $page)
                            <tr>
                                <td>{{$page->id}}</td>
                                <td>{{$page->name}}</td>
                                <td>{{$page->fulluri}}</td>
                                <td>{{$page->sort}}</td>
                                <td>{{$page->status}}</td>
                                <td>{{$page->created_at}}</td>
                                <td>{{$page->updated_at}}</td>
                                <td>
                                    <a href="{{route('admin.page.show', $page->id)}}"
                                       class="btn btn-xs btn-info"><span
                                            class="fa fa-eye"></span> {{__('admin.TTL_VIEW')}}</a>
                                    <a href="{{route('admin.page.edit', $page->id)}}"
                                       class="btn btn-xs btn-success"><span
                                            class="fa fa-edit"></span> {{__('admin.TTL_EDIT')}}</a>
                                    <form method="POST" action="{{ route('admin.page.destroy', $page) }}"
                                          class="inline">
                                        @csrf
                                        @method('DELETE')
                                        <button class="btn btn-xs btn-danger" onclick="return confirm('{{__('admin.TTL_DELETE_SURE')}}')"><span
                                                class="fa fa-remove"></span> {{__('admin.TTL_DELETE')}}
                                        </button>
                                    </form>

                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                        <tfoot>
                        <tr>
                            <th>ID</th>
                            <th>{{__('admin.pages.ATR_NAME')}}</th>
                            <th>{{__('admin.pages.ATR_URI')}}</th>
                            <th>{{__('admin.TTL_SORT')}}</th>
                            <th>{{__('admin.TTL_STATUS')}}</th>
                            <th>{{__('admin.TTL_CREATED_AT')}}</th>
                            <th>{{__('admin.TTL_UPDATED_AT')}}</th>
                            <th>{{__('admin.TTL_ACTIONS')}}</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>

@endsection
