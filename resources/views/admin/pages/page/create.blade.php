@extends('admin.layouts.onecolumn')

@section('header', __('admin.pages.TTL_NEW_PAGE'))
@section('breadcrumbs', Breadcrumbs::render())
@section('main_block_content')

    <div class="row">
        <div class="col-xs-12">
            <div class="box">

                @include('admin.pages.page._form', [
                'mode' => 'create',
                'page' => $page,
                'tree' => $tree,
                'menus' => $menus,
                'action' => route('admin.page.store')])

            </div>
        </div>
    </div>

@endsection
