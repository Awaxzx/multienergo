<form action="{{$action}}" method="post">
    @csrf
    @if(isset($method) && $method == 'put')
        @method('PUT')
    @endif
    <div class="box-body">

    @include('admin.page_elements._errors_list')

    <!-- Custom Tabs -->
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab_page" data-toggle="tab">{{__('admin.pages.TTL_PAGE')}}</a></li>
                <li><a href="#tab_meta" data-toggle="tab">{{__('admin.meta.TTL_META')}}</a></li>
                @if($mode != 'create')
                    <li><a href="#tab_files" data-toggle="tab">{{__('admin.files.TTL_FILES')}}</a></li>
                @endif
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="tab_page">


                    <div class="form-group{{ $errors->has('name') ? ' has-error' : ' has-success' }}">
                        <label for="name">{{__('admin.pages.ATR_NAME')}}</label>
                        <input type="text" id="name" name="name" value="{{ old('name', $page->name) }}"
                               class="form-control"
                               placeholder=""/>
                        @if ($errors->has('name'))
                            <span class="help-block"><strong>{{ $errors->first('name') }}</strong></span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->has('parent_id') ? ' has-error' : ' has-success' }}">
                        <label for="parent_id">{{__('admin.pages.ATR_PARENT_PAGE')}}</label>
                        <select class="form-control" name="parent_id" id="parent_id">
                            <option value="0" @if($page->parent_id == 0) selected @endif>/</option>
                            @foreach ($tree as $page_id => $item)
                                <option value="{{ $page_id }}"
                                        @if($page->parent_id == $page_id) selected @endif>{{ $item['offset']}}{{ $item['name']}}</option>
                            @endforeach
                        </select>

                        @if ($errors->has('parent_id'))
                            <span class="help-block"><strong>{{ $errors->first('parent_id') }}</strong></span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->has('uri') ? ' has-error' : ' has-success' }}">
                        <label for="name">{{__('admin.pages.ATR_URI')}}</label>
                        <input type="text" id="uri" name="uri" value="{{ old('uri', $page->uri) }}" class="form-control"
                               placeholder=""/>
                        @if ($errors->has('uri'))
                            <span class="help-block"><strong>{{ $errors->first('uri') }}</strong></span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->has('content') ? ' has-error' : ' has-success' }}">
                        <label for="name">{{__('admin.pages.ATR_CONTENT')}}</label>
                        <textarea id="content" name="content" class="form-control htmleditor">{{ old('content', $page->content) }}</textarea>
                        @if ($errors->has('content'))
                            <span class="help-block"><strong>{{ $errors->first('content') }}</strong></span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->has('menu') ? ' has-error' : ' has-success' }}">
                        <label for="">{{__('admin.pages.ATR_DISPLAY_IN_MENU')}}</label>

                        @foreach($menus as $menu)
                            @php($checked = '')
                            @if(old('menu'))

                                @foreach(old('menu') as $pm)
                                    @if($pm == $menu->id)
                                        @php($checked = 'checked')
                                    @endif
                                @endforeach

                            @elseif($page->menus)

                                @foreach($page->menus as $pm)
                                    @if($pm->id == $menu->id)
                                        @php($checked = 'checked')
                                    @endif
                                @endforeach

                            @endif
                            <div class="checkbox">
                                <label for="menu_{{$menu->id}}">
                                    <input type="checkbox" name="menu[]" value="{{$menu->id}}"
                                           id="menu_{{$menu->id}}" {{$checked}}/>
                                    {{$menu->name}}
                                </label>
                            </div>
                        @endforeach
                    </div>
                    <div class="form-group{{ $errors->has('sort') ? ' has-error' : ' has-success' }}">
                        <label for="sort">{{__('admin.ATR_SORT')}}</label>
                        <input type="text" class="form-control" name="sort" id="sort"
                               value="{{ old('sort', $page->sort) }}"
                               placeholder="">
                        @if ($errors->has('sort'))
                            <span class="help-block"><strong>{{ $errors->first('sort') }}</strong></span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->has('status') ? ' has-error' : ' has-success' }}">
                        <label for="status">{{__('admin.ATR_STATUS')}}</label>
                        <select class="form-control" name="status" id="status">
                            @foreach (\App\Models\Page::statuses() as $status)
                                <option value="{{ $status }}"
                                        @if($status == $page->status) selected="selected" @endif>{{ __("status.$status") }}</option>
                            @endforeach;
                        </select>

                        @if ($errors->has('status'))
                            <span class="help-block"><strong>{{ $errors->first('status') }}</strong></span>
                        @endif
                    </div>


                </div>
                <!-- /.tab-pane -->
                <div class="tab-pane" id="tab_meta">
                    @include('admin.meta._form', ['meta' => $page->getMeta()])
                </div>
                <!-- /.tab-pane -->
                @if($mode != 'create')
                    <div class="tab-pane" id="tab_files">

                        @include('admin.files._list', ['files' => $page->getFiles(),
                        'owner_id' => $page->id,
                        'owner_class' => get_class($page)
                        ])

                    </div>
            @endif
            <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
        </div>
        <!-- nav-tabs-custom -->

    </div>
    <!-- /.box-body -->


    <div class="box-footer">
        @include('admin.page_elements._buttons', ['mode' => $mode, 'cancel_route' => 'admin.page'])
    </div>
</form>
@include('admin.files._modal_form')