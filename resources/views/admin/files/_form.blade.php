<div class="form-group{{ $errors->has('file.title') ? ' has-error' : ' has-success' }}">
    <label for="file_title">{{__('admin.files.ATR_TITLE')}}</label>
    <input type="text" id="file_title" name="file[title]"
           value="{{ old('file.title', $file->title) }}"
           class="form-control"
           placeholder="Enter File title"/>
    @if ($errors->has('file.title'))
        <span class="help-block"><strong>{{ $errors->first('file.title') }}</strong></span>
    @endif
</div>
<div class="form-group{{ $errors->has('file.description') ? ' has-error' : ' has-success' }}">
    <label for="file_description">{{__('admin.files.ATR_DESCRIPTION')}}</label>
    <textarea id="file_description" name="file[description]"
              class="form-control">{{ old('file.description', $file->description)}}</textarea>
    @if ($errors->has('file.description'))
        <span class="help-block"><strong>{{ $errors->first('file.description') }}</strong></span>
    @endif
</div>
<div class="form-group{{ $errors->has('file.name') ? ' has-error' : ' has-success' }}">
    <label for="file_name">{{__('admin.files.ATR_FILE')}}</label>
    <input type="file" id="file_name" name="file[name]" value="" class="form-control"/>
    @if ($errors->has('file.name'))
        <span class="help-block"><strong>{{ $errors->first('file.name') }}</strong></span>
    @endif
</div>
<div class="form-group{{ $errors->has('file.sort') ? ' has-error' : ' has-success' }}">
    <label for="file_sort">{{__('admin.ATR_SORT')}}</label>
    <input type="text" class="form-control" name="file[sort]" id="file_sort"
           value="{{ old('file.sort', $file->sort) }}"
           placeholder="Enter sort">
    @if ($errors->has('file.sort'))
        <span class="help-block"><strong>{{ $errors->first('file.sort') }}</strong></span>
    @endif
</div>
<div class="form-group{{ $errors->has('file.status') ? ' has-error' : ' has-success' }}">
    <label for="file_status">{{__('admin.ATR_STATUS')}}</label>
    <select class="form-control" name="file[status]" id="file_status">
        @foreach (\App\Models\File::statuses() as $status)
            <option value="{{ $status }}"
                    @if($status == $file->status) selected="selected" @endif>{{ __("status.$status") }}</option>
        @endforeach;
    </select>

    @if ($errors->has('file.status'))
        <span class="help-block"><strong>{{ $errors->first('file.status') }}</strong></span>
    @endif
</div>
<input type="hidden" id="type" name="file[type]" value="{{$file->type}}"/>
<input type="hidden" id="file_owner_id" name="file[owner_id]" value="{{$file->owner_id}}"/>
<input type="hidden" id="file_owner_class" name="file[owner_class]" value="{{$file->owner_class}}"/>