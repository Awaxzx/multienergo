<div class="text-right">@include('admin.files._add-button', ['owner_id' => $owner_id, 'owner_class' => $owner_class])</div>
<table class="table table-bordered table-striped">
    <thead>
    <tr>
        <th>ID</th>
        <th>{{__('admin.files.ATR_FILENAME')}}</th>
        <th>{{__('admin.files.ATR_TYPE')}}</th>
        <th>{{__('admin.files.ATR_TITLE')}}</th>
        <th>{{__('admin.files.ATR_DESCRIPTION')}}</th>
        <th>{{__('admin.ATR_SORT')}}</th>
        <th>{{__('admin.ATR_STATUS')}}</th>
        <th>{{__('admin.ATR_CREATED_AT')}}</th>
        <th>{{__('admin.ATR_UPDATED_AT')}}</th>
        <th>{{__('admin.TTL_ACTIONS')}}</th>
    </tr>
    </thead>
    <tbody>

    @if(count($files) == 0)
        <tr>
            <td colspan="10">
                <div class="text-center">{{__('admin.files.TTL_NO_FILES')}}</div>
            </td>
        </tr>
    @endif

    @foreach($files as $file)
            <tr>
                <td>{{$file->id}}</td>
                <td>{{$file->name}}</td>
                <td>{{$file->type}}</td>
                <td>{{$file->title}}</td>
                <td>{{$file->description}}</td>
                <td>{{$file->sort}}</td>
                <td>{{$file->status}}</td>
                <td>{{$file->created_at}}</td>
                <td>{{$file->updated_at}}</td>
                <td>
                    <a href="#"
                       data-toggle="modal"
                       data-target="#file-modal"
                       data-get="{{route('admin.files.edit', $file->id)}}"
                       class="btn btn-xs btn-success"><span class="fa fa-edit"></span> {{__('admin.TTL_EDIT')}}</a>
                    <a href="#"
                       class="btn btn-xs btn-danger remove-file"
                       data-action="{{ route('admin.files.destroy', $file) }}"
                       ><span class="fa fa-remove"></span> {{__('admin.TTL_DELETE')}}</a>
                </td>
            </tr>
        @endforeach


    </tbody>

</table>
<div class="text-right">@include('admin.files._add-button', ['owner_id' => $owner_id, 'owner_class' => $owner_class])</div>

