<button type="button" class="btn btn-primary file-button"
        data-toggle="modal"
        data-target="#file-modal"
        data-owner_id="{{$owner_id}}"
        data-owner_class="{{$owner_class}}"
        data-get="{{route('admin.files.create')}}"><span
            class="fa fa-plus"></span> {{__('admin.files.TTL_ADD_FILE')}}
</button>