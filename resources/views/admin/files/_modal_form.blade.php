<div class="modal fade" id="file-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <form action="/" method="post" enctype="multipart/form-data" id="file-form-modal">
                @csrf
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">{{__('admin.files.TTL_ADD_FILE')}}</h4>
                </div>
                <div class="modal-body">


                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">{{__('admin.TTL_CLOSE')}}</button>
                    <button type="submit" class="btn btn-primary" id="modal-save">{{__('admin.TTL_SAVE')}}</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
@section('scripts')
    <script>

        // Load form to modal
        $('#file-modal').on('show.bs.modal', function (event) {

            var button = $(event.relatedTarget)
            var modal = $(this);


            $.ajax({
                url: button.attr('data-get'),
                method: 'get',
                dataType: 'json',
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                    'Accept': 'application/json'
                },
                success: function (result) {


                    $('#modal-save').show();
                    modal.find('.modal-title').html(result.title);
                    modal.find('.modal-body').html(result.form);
                    modal.find('.modal-content form').attr('action', result.action);
                    modal.find('.modal-content form').attr('method', result.method);
                    if (button.attr('data-owner_id')) {
                        modal.find('#file_owner_id').val(button.attr('data-owner_id'));
                        modal.find('#file_owner_class').val(button.attr('data-owner_class'));
                    }

                }
            });

        });

        // Send form
        $('#modal-save').on('click', function (event) {
            event.stopPropagation();
            event.preventDefault();

            var form_data = new FormData();
            if ($('#file-form-modal').attr('method') == 'put') {
                form_data.append('_method', 'put');
            }

            if($('#file_name').prop('files')[0] != undefined)
            {
                form_data.append('file[name]', $('#file_name').prop('files')[0]);
            }

            form_data.append('file[title]', $('#file_title').val());
            form_data.append('file[description]', $('#file_description').val());
            form_data.append('file[sort]', $('#file_sort').val());
            form_data.append('file[status]', $('#file_status > option:selected').val());
            form_data.append('file[owner_id]', $('#file_owner_id').val());
            form_data.append('file[owner_class]', $('#file_owner_class').val());



            $('.modal-content form').find('.help-block').remove();
            $('.modal-content form').find('.form-group').removeClass('has-error');

            $.ajax({
                url: $('#file-form-modal').attr('action'),
                method: 'post',
                dataType: 'json',
                cache: false,
                contentType: false,
                processData: false,
                data: form_data,
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                    'Accept': 'application/json'
                },
                success: function (response) {

                    loadFilesList();
                    $('.modal-body').html(response.success);
                    $('#modal-save').hide();


                },
                error: function (xhr, status, response) {

                    var error = $.parseJSON(xhr.responseText);

                    if ($.isEmptyObject(error.errors) == false) {

                        $.each(error.errors, function (key, value) {

                            $('#' + key.replace(".", "_"))
                                .closest('.form-group')
                                .addClass('has-error')
                                .append('<span class="help-block"><strong>' + value + '</strong></span>');
                        });
                    }

                }
            });

        });

        // Remove file
        $('body').on('click', '.remove-file', function(event){

            event.stopPropagation();
            event.preventDefault();
            if(confirm('Are you sure?'))
            {
                var form_data = new FormData();
                form_data.append('_method', 'delete');

                $.ajax({
                    url: $(this).attr('data-action'),
                    method: 'post',
                    dataType: 'json',
                    cache: false,
                    contentType: false,
                    processData: false,
                    data: form_data,
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                        'Accept': 'application/json'
                    },
                    success: function (response) {

                        loadFilesList();
                        alert(response.success);
                    }
                });
            }


        });

        // Load files list
        function loadFilesList()
        {
            $.ajax({
                url: '{{route('admin.files')}}',
                method: 'get',
                dataType: 'html',
                cache: false,
                contentType: false,
                data: 'owner_id='+$('.file-button').attr('data-owner_id')+'&owner_class='+$('.file-button').attr('data-owner_class'),
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content'),
                    'Accept': 'application/json'
                },
                success: function (response) {
                    $('#tab_files').html(response);
                }
            });
        }
    </script>
@endsection