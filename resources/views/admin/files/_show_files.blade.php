<dl class="dl-horizontal">
    <dt>{{__('admin.files.TTL_FILES')}}</dt>
    <dd>
        <table class="table table-bordered table-striped">
            <thead>
            <tr>
                <th>ID</th>
                <th>{{__('admin.files.ATR_FILENAME')}}</th>
                <th>{{__('admin.files.ATR_TYPE')}}</th>
                <th>{{__('admin.files.ATR_TITLE')}}</th>
                <th>{{__('admin.files.ATR_DESCRIPTION')}}</th>
                <th>{{__('admin.ATR_SORT')}}</th>
                <th>{{__('admin.ATR_STATUS')}}</th>
                <th>{{__('admin.ATR_CREATED_AT')}}</th>
                <th>{{__('admin.ATR_UPDATED_AT')}}</th>
            </tr>
            </thead>
            <tbody>

            @if(count($files) == 0)
                <tr>
                    <td colspan="10">
                        <div class="text-center">{{__('admin.files.TTL_NO_FILES')}}</div>
                    </td>
                </tr>
            @endif

            @foreach($files as $file)
                <tr>
                    <td>{{$file->id}}</td>
                    <td>{{$file->name}}</td>
                    <td>{{$file->type}}</td>
                    <td>{{$file->title}}</td>
                    <td>{{$file->description}}</td>
                    <td>{{$file->sort}}</td>
                    <td>{{$file->status}}</td>
                    <td>{{$file->created_at}}</td>
                    <td>{{$file->updated_at}}</td>
                </tr>
            @endforeach


            </tbody>

        </table>
    </dd>
</dl>