@extends('admin.layouts.onecolumn')

@section('header', __('admin.users.TTL_VIEW_USER'))
@section('breadcrumbs', Breadcrumbs::render())
@section('main_block_content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">

                <div class="box-body">
                    <div class="text-right">
                        <a href="{{route('admin.user')}}" class="btn btn-info"><span
                                class="fa fa-list"></span> {{__('admin.TTL_BACK_TO_LIST')}}</a>
                        <a href="{{route('admin.user.edit', $user->id)}}" class="btn btn-success"><span
                                class="fa fa-edit"></span> {{__('admin.TTL_EDIT')}}</a>
                        <form method="POST" action="{{ route('admin.user.destroy', $user) }}" class="inline">
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-danger" onclick="return confirm('{{__('admin.TTL_DELETE_SURE')}}')"><span
                                    class="fa fa-remove"></span> {{__('admin.TTL_DELETE')}}
                            </button>
                        </form>
                        <a href="{{route('admin.user.create')}}" class="btn btn-primary"><span
                                class="fa fa-plus"></span> {{__('admin.users.TTL_ADD_NEW_USER')}}</a>
                    </div>
                    <!-- Main content -->
                    <dl class="dl-horizontal">
                        <dt>ID</dt>
                        <dd>{{$user->id}}</dd>

                        <dt>{{__('admin.users.ATR_NAME')}}</dt>
                        <dd>{{$user->name}}</dd>

                        <dt>{{__('admin.users.ATR_EMAIL')}}</dt>
                        <dd>{{$user->email}}</dd>

                        {{--<dt>{{__('admin.ATR_STATUS')}}</dt>
                        <dd>{{$user->status}}</dd>--}}
                    </dl>
                </div>
            </div>
        </div>
    </div>
@endsection
