@extends('admin.layouts.onecolumn')

@section('header', __('admin.users.TTL_USERS'))
@section('breadcrumbs', Breadcrumbs::render())
@section('main_block_content')

<div class="row">
    <div class="col-xs-12">
        <div class="box">
            <div class="box-header">
                <div class="text-right"><a href="{{route('admin.user.create')}}" class="btn btn-primary"><span
                            class="fa fa-plus"></span> {{__('admin.users.TTL_ADD_NEW_USER')}}</a></div>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
                <table id="table1" class="table table-bordered table-striped">
                    <thead>
                    <tr>
                        <th>ID</th>
                        <th>{{__('admin.users.ATR_NAME')}}</th>
                        <th>{{__('admin.users.ATR_EMAIL')}}</th>
                        <th>{{__('admin.ATR_CREATED_AT')}}</th>
                        <th>{{__('admin.ATR_UPDATED_AT')}}</th>
                        <th>{{__('admin.ATR_ACTIONS')}}</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($users as $user)
                        <tr>
                            <td>{{$user->id}}</td>
                            <td>{{$user->name}}</td>
                            <td>{{$user->email}}</td>
                            <td>{{$user->created_at}}</td>
                            <td>{{$user->updated_at}}</td>
                            <td>
                                <a href="{{route('admin.user.show', $user->id)}}" class="btn btn-xs btn-info"><span
                                        class="fa fa-eye"></span> {{__('admin.TTL_VIEW')}}</a>
                                <a href="{{route('admin.user.edit', $user->id)}}" class="btn btn-xs btn-success"><span
                                        class="fa fa-edit"></span> {{__('admin.TTL_EDIT')}}</a>
                                <form method="POST" action="{{ route('admin.user.destroy', $user) }}" class="inline">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn btn-xs btn-danger" onclick="return confirm('{{__('admin.TTL_DELETE_SURE')}}')"><span
                                            class="fa fa-remove"></span> {{__('admin.TTL_DELETE')}}
                                    </button>
                                </form>

                            </td>
                        </tr>
                    @endforeach

                    </tbody>
                    <tfoot>
                    <tr>
                        <th>ID</th>
                        <th>{{__('admin.users.ATR_NAME')}}</th>
                        <th>{{__('admin.users.ATR_EMAIL')}}</th>
                        <th>{{__('admin.ATR_CREATED_AT')}}</th>
                        <th>{{__('admin.ATR_UPDATED_AT')}}</th>
                        <th>{{__('admin.ATR_ACTIONS')}}</th>
                    </tr>
                    </tfoot>
                </table>
            </div>
            <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
</div>

@endsection
