@extends('admin.layouts.onecolumn')

@section('header', __('admin.users.TTL_EDIT_USER'))
@section('breadcrumbs', Breadcrumbs::render())
@section('main_block_content')
    <!-- Main content -->

    <div class="row">
        <div class="col-xs-12">
            <div class="box">

                @include('admin.user._form', ['method' => 'put',
                'user' => $user,
                'mode' => 'update',
                'action' => route('admin.user.update', ['user' => $user])])

            </div>
        </div>
    </div>

@endsection
