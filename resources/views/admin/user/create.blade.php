@extends('admin.layouts.onecolumn')

@section('header', __('admin.users.TTL_NEW_USER'))
@section('breadcrumbs', Breadcrumbs::render())
@section('main_block_content')

    <div class="row">
        <div class="col-xs-12">
            <div class="box">

                @include('admin.user._form', ['user' => $user, 'action' => route('admin.user.store'), 'mode' => 'create'])

            </div>
        </div>
    </div>

@endsection
