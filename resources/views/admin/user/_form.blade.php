<form action="{{$action}}" method="post">
    @csrf
    @if(isset($method) && $method == 'put')
        @method('PUT')
    @endif
    <div class="box-body">
        <div class="form-group{{ $errors->has('name') ? ' has-error' : ' has-success' }}">
            <label for="user_name">{{__('admin.users.ATR_NAME')}}</label>
            <input type="text" id="user_name" name="name" value="{{ old('name', $user->name) }}" class="form-control" placeholder="" />
            @if ($errors->has('name'))
                <span class="help-block"><strong>{{ $errors->first('name') }}</strong></span>
            @endif
        </div>
        <div class="form-group{{ $errors->has('email') ? ' has-error' : ' has-success' }}">
            <label for="email">{{__('admin.users.ATR_EMAIL')}}</label>
            <input type="email" class="form-control" name="email" id="email" value="{{ old('email', $user->email) }}" placeholder="">
            @if ($errors->has('email'))
                <span class="help-block"><strong>{{ $errors->first('email') }}</strong></span>
            @endif
        </div>
        <div class="form-group{{ $errors->has('password') ? ' has-error' : ' has-success' }}">
            <label for="password">{{__('admin.users.ATR_PASSWORD')}}</label>
            <input type="password" class="form-control" name="password" id="password" placeholder="">
            @if ($errors->has('password'))
                <span class="help-block"><strong>{{ $errors->first('password') }}</strong></span>
            @endif
        </div>
        <div class="form-group{{ $errors->has('password_confirmation') ? ' has-error' : ' has-success' }}">
            <label for="password">{{__('admin.users.ATR_CONFIRM_PASSWORD')}}</label>
            <input type="password" class="form-control" name="password_confirmation" id="password_confirm" placeholder="">
            @if ($errors->has('password_confirmation'))
                <span class="help-block"><strong>{{ $errors->first('password_confirmation') }}</strong></span>
            @endif
        </div>
    </div>
    <!-- /.box-body -->

    <div class="box-footer">
        @include('admin.page_elements._buttons', ['mode' => $mode, 'cancel_route' => 'admin.user'])
    </div>
</form>
