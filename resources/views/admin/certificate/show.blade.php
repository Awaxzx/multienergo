@extends('admin.layouts.onecolumn')

@section('header', __('admin.certificates.TTL_VIEW_CERTIFICATE'))
@section('breadcrumbs', Breadcrumbs::render())
@section('main_block_content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">

                <div class="box-body">
                    <div class="text-right">
                        <a href="{{route('admin.certificate')}}" class="btn btn-info"><span
                                class="fa fa-list"></span> {{__('admin.TTL_BACK_TO_LIST')}}</a>
                        <a href="{{route('admin.certificate.edit', $certificate->id)}}" class="btn btn-success"><span
                                class="fa fa-edit"></span> {{__('admin.TTL_EDIT')}}</a>
                        <form method="POST" action="{{ route('admin.certificate.destroy', $certificate) }}" class="inline">
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-danger" onclick="return confirm('{{__('admin.TTL_DELETE_SURE')}}')"><span
                                    class="fa fa-remove"></span> {{__('admin.TTL_DELETE')}}
                            </button>
                        </form>
                        <a href="{{route('admin.certificate.create')}}" class="btn btn-primary"><span
                                class="fa fa-plus"></span> {{__('admin.certificates.TTL_ADD_NEW_CERTIFICATE')}}</a>
                    </div>
                    <!-- Main content -->
                    <dl class="dl-horizontal">
                        <dt>ID</dt>
                        <dd>{{$certificate->id}}</dd>

                        <dt>{{__('admin.certificates.ATR_NAME')}}</dt>
                        <dd>{{$certificate->name}}</dd>

                        <dt>{{__('admin.certificates.ATR_IMAGE')}}</dt>
                        <dd>
                            @isset($certificate->image)
                                <img src="{{ asset('storage/'.$certificate->getThumbnail())}}" alt=""><br>
                                <img src="{{ asset('storage/'.$certificate->getImage())}}" alt="">
                            @endisset
                        </dd>

                        <dt>{{__('admin.ATR_SORT')}}</dt>
                        <dd>{{$certificate->sort}}</dd>

                        <dt>{{__('admin.ATR_STATUS')}}</dt>
                        <dd>{{$certificate->status}}</dd>
                    </dl>
                </div>
            </div>
        </div>
    </div>
@endsection
