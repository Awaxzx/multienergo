<form action="{{$action}}" method="post" enctype="multipart/form-data">
    @csrf
    @if(isset($method) && $method == 'put')
        @method('PUT')
    @endif
    <div class="box-body">
        <div class="form-group{{ $errors->has('name') ? ' has-error' : ' has-success' }}">
            <label for="name">{{__('admin.certificates.ATR_NAME')}}</label>
            <input type="text" id="name" name="name" value="{{ old('name', $certificate->name) }}" class="form-control"
                   placeholder=""/>
            @if ($errors->has('name'))
                <span class="help-block"><strong>{{ $errors->first('name') }}</strong></span>
            @endif
        </div>
        <div class="form-group{{ $errors->has('image') ? ' has-error' : ' has-success' }}">
            <label for="name">{{__('admin.certificates.ATR_IMAGE')}}</label>
            @isset($certificate->image)
                <img src="{{ asset('storage/'.$certificate->getImage())}}" alt="">
            @endisset
            <input type="file" id="image" name="image" value="{{ old('image', $certificate->image) }}" />
            @if ($errors->has('image'))
                <span class="help-block"><strong>{{ $errors->first('image') }}</strong></span>
            @endif
        </div>
        <div class="form-group{{ $errors->has('sort') ? ' has-error' : ' has-success' }}">
            <label for="sort">{{__('admin.ATR_SORT')}}</label>
            <input type="text" class="form-control" name="sort" id="sort" value="{{ old('sort', $certificate->sort) }}"
                   placeholder="">
            @if ($errors->has('sort'))
                <span class="help-block"><strong>{{ $errors->first('sort') }}</strong></span>
            @endif
        </div>
        <div class="form-group{{ $errors->has('status') ? ' has-error' : ' has-success' }}">
            <label for="status">{{__('admin.ATR_STATUS')}}</label>
            <select class="form-control" name="status" id="status">
                @foreach (\App\Models\Certificate::statuses() as $status)
                    <option value="{{ $status }}"
                            @if($status == $certificate->status) selected="selected" @endif>{{ __("status.$status") }}</option>
                @endforeach;
            </select>

            @if ($errors->has('status'))
                <span class="help-block"><strong>{{ $errors->first('status') }}</strong></span>
            @endif
        </div>

    </div>
    <!-- /.box-body -->

    <div class="box-footer">
        @include('admin.page_elements._buttons', ['mode' => $mode, 'cancel_route' => 'admin.certificate'])
    </div>
</form>