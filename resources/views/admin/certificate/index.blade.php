@extends('admin.layouts.onecolumn')

@section('header', __('admin.certificates.TTL_CERTIFICATES'))
@section('breadcrumbs', Breadcrumbs::render())
@section('main_block_content')

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <div class="text-right"><a href="{{route('admin.certificate.create')}}" class="btn btn-primary"><span
                                class="fa fa-plus"></span> {{__('admin.certificates.TTL_ADD_NEW_CERTIFICATE')}}</a></div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="table1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>{{__('admin.certificates.ATR_NAME')}}</th>
                            <th>{{__('admin.certificates.ATR_THUMBNAIL')}}</th>
                            <th>{{__('admin.certificates.ATR_IMAGE')}}</th>
                            <th>{{__('admin.ATR_SORT')}}</th>
                            <th>{{__('admin.ATR_STATUS')}}</th>
                            <th>{{__('admin.ATR_CREATED_AT')}}</th>
                            <th>{{__('admin.ATR_UPDATED_AT')}}</th>
                            <th>{{__('admin.ATR_ACTIONS')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($certificates as $certificate)
                            <tr>
                                <td>{{$certificate->id}}</td>
                                <td>{{$certificate->name}}</td>
                                <td>{!! ($certificate->image)?'<a class="fancybox" href="'.asset('storage/'.$certificate->getThumbnail()).'"><img src="'.asset('storage/'.$certificate->getThumbnail()).'" style="width:10rem" /></a>':''!!}</td>
                                <td>{!! ($certificate->image)?'<a class="fancybox" href="'.asset('storage/'.$certificate->getImage()).'"><img src="'.asset('storage/'.$certificate->getImage()).'" style="width:10rem" /></a>':''!!}</td>
                                <td>{{$certificate->sort}}</td>
                                <td>{{$certificate->status}}</td>
                                <td>{{$certificate->created_at}}</td>
                                <td>{{$certificate->updated_at}}</td>
                                <td>
                                    <a href="{{route('admin.certificate.show', $certificate->id)}}"
                                       class="btn btn-xs btn-info"><span
                                            class="fa fa-eye"></span> {{__('admin.TTL_VIEW')}}</a>
                                    <a href="{{route('admin.certificate.edit', $certificate->id)}}"
                                       class="btn btn-xs btn-success"><span
                                            class="fa fa-edit"></span> {{__('admin.TTL_EDIT')}}</a>
                                    <form method="POST" action="{{ route('admin.certificate.destroy', $certificate) }}"
                                          class="inline">
                                        @csrf
                                        @method('DELETE')
                                        <button class="btn btn-xs btn-danger" onclick="return confirm('{{__('admin.TTL_DELETE_SURE')}}')"><span
                                                class="fa fa-remove"></span> {{__('admin.TTL_DELETE')}}
                                        </button>
                                    </form>

                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                        <tfoot>
                        <tr>
                            <th>ID</th>
                            <th>{{__('admin.certificates.ATR_NAME')}}</th>
                            <th>{{__('admin.certificates.ATR_THUMBNAIL')}}</th>
                            <th>{{__('admin.certificates.ATR_IMAGE')}}</th>
                            <th>{{__('admin.ATR_SORT')}}</th>
                            <th>{{__('admin.ATR_STATUS')}}</th>
                            <th>{{__('admin.ATR_CREATED_AT')}}</th>
                            <th>{{__('admin.ATR_UPDATED_AT')}}</th>
                            <th>{{__('admin.ATR_ACTIONS')}}</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>

@endsection
