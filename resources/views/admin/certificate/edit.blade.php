@extends('admin.layouts.onecolumn')

@section('header', __('admin.certificates.TTL_EDIT_CERTIFICATE'))
@section('breadcrumbs', Breadcrumbs::render())
@section('main_block_content')
    <!-- Main content -->

    <div class="row">
        <div class="col-xs-12">
            <div class="box">

                @include('admin.certificate._form', [
                'method' => 'put',
                'certificate' => $certificate,
                'mode' => 'update',
                'action' => route('admin.certificate.update', ['certificate' => $certificate])
                ])

            </div>
        </div>
    </div>

@endsection
