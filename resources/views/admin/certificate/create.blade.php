@extends('admin.layouts.onecolumn')

@section('header', __('admin.certificates.TTL_NEW_CERTIFICATE'))
@section('breadcrumbs', Breadcrumbs::render())
@section('main_block_content')

    <div class="row">
        <div class="col-xs-12">
            <div class="box">

                @include('admin.certificate._form', ['certificate' => $certificate,
                'mode' => 'create',
                'action' => route('admin.certificate.store')])

            </div>
        </div>
    </div>

@endsection
