@extends('admin.layouts.admin')

@section('content')

    <section class="content-header">
        <h1>@yield('header')</h1>
        @yield('breadcrumbs')
    </section>

    <!-- Main content -->
    <section class="content container-fluid">

        <!--------------------------
          | Your Page Content Here |
          -------------------------->
        @yield('main_block_content')
    </section>

@endsection
