<div class="form-group{{ $errors->has('meta.title') ? ' has-error' : ' has-success' }}">
    <label for="meta_title">{{__('admin.meta.ATR_TITLE')}}</label>
    <input type="text" id="meta_title" name="meta[title]" value="{{ old('meta.title', $meta->title) }}"
           class="form-control"
           placeholder=""/>
    @if ($errors->has('meta.title'))
        <span class="help-block"><strong>{{ $errors->first('meta.title') }}</strong></span>
    @endif
</div>
<div class="form-group{{ $errors->has('meta.keywords') ? ' has-error' : ' has-success' }}">
    <label for="meta_keywords">{{__('admin.meta.ATR_KEYWORDS')}}</label>
    <textarea id="meta_keywords" name="meta[keywords]"
           class="form-control">{{ old('meta.keywords', $meta->keywords) }}</textarea>
    @if ($errors->has('meta.keywords'))
        <span class="help-block"><strong>{{ $errors->first('meta.keywords') }}</strong></span>
    @endif
</div>
<div class="form-group{{ $errors->has('meta.description') ? ' has-error' : ' has-success' }}">
    <label for="meta_description">{{__('admin.meta.ATR_DESCRIPTION')}}</label>
    <textarea id="meta_description" name="meta[description]"
              class="form-control">{{ old('meta.description', $meta->description) }}</textarea>
    @if ($errors->has('meta.description'))
        <span class="help-block"><strong>{{ $errors->first('meta.description') }}</strong></span>
    @endif
</div>
