<dl class="dl-horizontal">
    <dt>{{__('admin.meta.ATR_TITLE')}}</dt>
    <dd>{{$item->getMetaTitle()}}</dd>

    <dt>{{__('admin.meta.ATR_KEYWORDS')}}</dt>
    <dd>{{$item->getMetaKeywords()}}</dd>

    <dt>{{__('admin.meta.ATR_DESCRIPTION')}}</dt>
    <dd>{{$item->getMetaDescription()}}</dd>
</dl>