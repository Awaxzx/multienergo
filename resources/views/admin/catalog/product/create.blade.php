@extends('admin.layouts.onecolumn')

@section('header', __('admin.products.TTL_NEW_PRODUCT'))
@section('breadcrumbs', Breadcrumbs::render())
@section('main_block_content')

    <div class="row">
        <div class="col-xs-12">
            <div class="box">


                @include('admin.catalog.product._form', [
                                                            'product' => $product,
                                                            'mode' => 'create',
                                                            'tree' => $tree,
                                                            'action' => route('admin.product.store'),
                                                          ])

            </div>
        </div>
    </div>

@endsection
