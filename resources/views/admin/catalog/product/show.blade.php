@extends('admin.layouts.onecolumn')

@section('header', __('admin.products.TTL_VIEW_PRODUCT'))
@section('breadcrumbs', Breadcrumbs::render())
@section('main_block_content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">

                <!-- /.box-header -->
                <div class="box-body">
                    <div class="text-right">
                        <a href="{{route('admin.product')}}" class="btn btn-info"><span
                                class="fa fa-list"></span> {{__('admin.TTL_BACK_TO_LIST')}}</a>
                        <a href="{{route('admin.product.edit', $product->id)}}" class="btn btn-success"><span
                                class="fa fa-edit"></span> {{__('admin.TTL_EDIT')}}</a>
                        <form method="POST" action="{{ route('admin.product.destroy', $product) }}" class="inline">
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-danger" onclick="return confirm('{{__('admin.TTL_DELETE_SURE')}}')"><span
                                    class="fa fa-remove"></span> {{__('admin.TTL_DELETE')}}
                            </button>
                        </form>
                        <a href="{{route('admin.product.create')}}" class="btn btn-primary"><span
                                class="fa fa-plus"></span> {{__('admin.products.TTL_ADD_NEW_PRODUCT')}}</a>
                    </div>
                    <!-- Main content -->
                    <dl class="dl-horizontal">
                        <dt>ID</dt>
                        <dd>{{$product->id}}</dd>

                        <dt>{{__('admin.products.ATR_CATEGORY')}}</dt>
                        <dd>{{$product->category->name}}</dd>

                        <dt>{{__('admin.products.ATR_NAME')}}</dt>
                        <dd>{{$product->name}}</dd>

                        <dt>{{__('admin.products.ATR_URI')}}</dt>
                        <dd>{{$product->uri}}</dd>

                        <dt>{{__('admin.products.ATR_SHORT_CONTENT')}}</dt>
                        <dd>{{$product->short_content}}</dd>

                        <dt>{{__('admin.products.ATR_CONTENT')}}</dt>
                        <dd>{{$product->content}}</dd>

                        <dt>{{__('admin.products.ATR_IMAGE')}}</dt>
                        <dd>
                            @isset($product->image)
                                <img src="{{ asset('storage/'.$product->getThumbnail())}}" alt=""><br/>
                                <img src="{{ asset('storage/'.$product->getImage())}}" alt="">
                            @endisset
                        </dd>

                        <dt>{{__('admin.ATR_SORT')}}</dt>
                        <dd>{{$product->sort}}</dd>

                        <dt>{{__('admin.ATR_STATUS')}}</dt>
                        <dd>{{$product->status}}</dd>
                    </dl>

                    @include('admin.meta._show_meta', ['item' => $product])
                    @include('admin.files._show_files', ['files' => $product->getFiles()])
                </div>
            </div>
        </div>
    </div>
@endsection
