@extends('admin.layouts.onecolumn')

@section('header', __('admin.products.TTL_PRODUCTS'))
@section('breadcrumbs', Breadcrumbs::render())
@section('main_block_content')

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <div class="text-right"><a href="{{route('admin.product.create')}}" class="btn btn-primary"><span
                                class="fa fa-plus"></span> {{__('admin.products.TTL_ADD_NEW_PRODUCT')}}</a></div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="table1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>{{__('admin.products.ATR_CATEGORY')}}</th>
                            <th>{{__('admin.products.ATR_NAME')}}</th>
                            <th>{{__('admin.products.ATR_URI')}}</th>
                            <th>{{__('admin.products.ATR_THUMBNAIL')}}</th>
                            <th>{{__('admin.products.ATR_IMAGE')}}</th>
                            <th>{{__('admin.ATR_SORT')}}</th>
                            <th>{{__('admin.ATR_STATUS')}}</th>
                            <th>{{__('admin.ATR_CREATED_AT')}}</th>
                            <th>{{__('admin.ATR_UPDATED_AT')}}</th>
                            <th>{{__('admin.ATR_ACTIONS')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($products as $product)
                            <tr>
                                <td>{{$product->id}}</td>
                                <td>{{$product->category->name}}</td>
                                <td>{{$product->name}}</td>
                                <td>{{$product->uri}}</td>
                                <td>{!! ($product->image)?'<a class="fancybox" href="'.asset('storage/'.$product->getThumbnail()).'"><img src="'.asset('storage/'.$product->getThumbnail()).'" style="width:10rem" /></a>':''!!}</td>
                                <td>{!! ($product->image)?'<a class="fancybox" href="'.asset('storage/'.$product->getImage()).'"><img src="'.asset('storage/'.$product->getImage()).'" style="width:10rem" /></a>':''!!}</td>
                                <td>{{$product->sort}}</td>
                                <td>{{$product->status}}</td>
                                <td>{{$product->created_at}}</td>
                                <td>{{$product->updated_at}}</td>
                                <td>
                                    <a href="{{route('admin.product.show', $product->id)}}"
                                       class="btn btn-xs btn-info"><span
                                            class="fa fa-eye"></span> {{__('admin.TTL_VIEW')}}</a>
                                    <a href="{{route('admin.product.edit', $product->id)}}"
                                       class="btn btn-xs btn-success"><span
                                            class="fa fa-edit"></span> {{__('admin.TTL_EDIT')}}</a>
                                    <form method="POST" action="{{ route('admin.product.destroy', $product) }}"
                                          class="inline">
                                        @csrf
                                        @method('DELETE')
                                        <button class="btn btn-xs btn-danger" onclick="return confirm('{{__('admin.TTL_DELETE_SURE')}}')"><span
                                                class="fa fa-remove"></span> {{__('admin.TTL_DELETE')}}
                                        </button>
                                    </form>

                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                        <tfoot>
                        <tr>
                            <th>ID</th>
                            <th>{{__('admin.products.ATR_CATEGORY')}}</th>
                            <th>{{__('admin.products.ATR_NAME')}}</th>
                            <th>{{__('admin.products.ATR_URI')}}</th>
                            <th>{{__('admin.products.ATR_THUMBNAIL')}}</th>
                            <th>{{__('admin.products.ATR_IMAGE')}}</th>
                            <th>{{__('admin.ATR_SORT')}}</th>
                            <th>{{__('admin.ATR_STATUS')}}</th>
                            <th>{{__('admin.ATR_CREATED_AT')}}</th>
                            <th>{{__('admin.ATR_UPDATED_AT')}}</th>
                            <th>{{__('admin.ATR_ACTIONS')}}</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>

@endsection
