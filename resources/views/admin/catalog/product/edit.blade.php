@extends('admin.layouts.onecolumn')

@section('header', __('admin.products.TTL_EDIT_PRODUCT'))
@section('breadcrumbs', Breadcrumbs::render())
@section('main_block_content')
    <!-- Main content -->

    <div class="row">
        <div class="col-xs-12">
            <div class="box">


                @include('admin.catalog.product._form', ['method' => 'put',
                                                          'mode' => 'update',
                                                          'product' => $product,
                                                          'tree' => $tree,
                                                          'action' => route('admin.product.update', ['product' => $product])])

            </div>
        </div>
    </div>

@endsection
