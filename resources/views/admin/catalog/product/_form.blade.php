<form action="{{$action}}" method="post" enctype="multipart/form-data">
    @csrf
    @if(isset($method) && $method == 'put')
        @method('PUT')
    @endif
    <div class="box-body">

    @include('admin.page_elements._errors_list')

    <!-- Custom Tabs -->
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab_product" data-toggle="tab">{{__('admin.products.TTL_PRODUCT')}}</a></li>
                <li><a href="#tab_meta" data-toggle="tab">{{__('admin.meta.TTL_META')}}</a></li>
                @if($mode != 'create')
                    <li><a href="#tab_files" data-toggle="tab">{{__('admin.files.TTL_FILES')}}</a></li>
                @endif
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="tab_product">

                    <div class="form-group{{ $errors->has('category_id') ? ' has-error' : ' has-success' }}">
                        <label for="category_id">{{__('admin.products.ATR_CATEGORY')}}</label>
                        <select class="form-control" name="category_id" id="category_id">
                            <option value="0" @if($product->category_id == 0) selected @endif>/</option>
                            @foreach ($tree as $category_id => $categ)
                                <option value="{{ $category_id }}"
                                        @if(old('category_id', $product->category_id) == $category_id) selected @endif>{{ $categ['offset']}}{{ $categ['name']}}</option>
                            @endforeach
                        </select>

                        @if ($errors->has('category_id'))
                            <span class="help-block"><strong>{{ $errors->first('category_id') }}</strong></span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->has('name') ? ' has-error' : ' has-success' }}">
                        <label for="name">{{__('admin.products.ATR_NAME')}}</label>
                        <input type="text" id="name" name="name" value="{{ old('name', $product->name) }}" class="form-control"
                               placeholder=""/>
                        @if ($errors->has('name'))
                            <span class="help-block"><strong>{{ $errors->first('name') }}</strong></span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->has('uri') ? ' has-error' : ' has-success' }}">
                        <label for="name">{{__('admin.products.ATR_URI')}}</label>
                        <input type="text" id="uri" name="uri" value="{{ old('uri', $product->uri) }}" class="form-control"
                               placeholder=""/>
                        @if ($errors->has('uri'))
                            <span class="help-block"><strong>{{ $errors->first('uri') }}</strong></span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->has('short_content') ? ' has-error' : ' has-success' }}">
                        <label for="name">{{__('admin.products.ATR_SHORT_CONTENT')}}</label>
                        <textarea id="short_content" name="short_content" class="form-control htmleditor">{{ old('short_content', $product->short_content) }}</textarea>
                        @if ($errors->has('short_content'))
                            <span class="help-block"><strong>{{ $errors->first('short_content') }}</strong></span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->has('content') ? ' has-error' : ' has-success' }}">
                        <label for="name">{{__('admin.products.ATR_CONTENT')}}</label>
                        <textarea id="content" name="content" class="form-control htmleditor">{{ old('content', $product->content) }}</textarea>
                        @if ($errors->has('content'))
                            <span class="help-block"><strong>{{ $errors->first('content') }}</strong></span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->has('image') ? ' has-error' : ' has-success' }}">
                        <label for="name">{{__('admin.products.ATR_IMAGE')}}</label>
                        @isset($product->image)
                            <img src="{{ asset('storage/'.$product->getThumbnail())}}" alt=""><br/>
                            <img src="{{ asset('storage/'.$product->getImage())}}" alt="">
                        @endisset
                        <input type="file" id="image" name="image" value="{{ old('image', $product->image) }}"/>
                        @if ($errors->has('image'))
                            <span class="help-block"><strong>{{ $errors->first('image') }}</strong></span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->has('sort') ? ' has-error' : ' has-success' }}">
                        <label for="sort">{{__('admin.ATR_SORT')}}</label>
                        <input type="text" class="form-control" name="sort" id="sort" value="{{ old('sort', $product->sort) }}"
                               placeholder="Enter sort">
                        @if ($errors->has('sort'))
                            <span class="help-block"><strong>{{ $errors->first('sort') }}</strong></span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->has('status') ? ' has-error' : ' has-success' }}">
                        <label for="status">{{__('admin.ATR_STATUS')}}</label>
                        <select class="form-control" name="status" id="status">
                            @foreach (\App\Models\Product::statuses() as $status)
                                <option value="{{ $status }}"
                                        @if($status == $product->status) selected @endif>{{ __("status.$status") }}</option>
                            @endforeach;
                        </select>

                        @if ($errors->has('status'))
                            <span class="help-block"><strong>{{ $errors->first('status') }}</strong></span>
                        @endif
                    </div>


                </div>
                <!-- /.tab-pane -->
                <div class="tab-pane" id="tab_meta">
                    @include('admin.meta._form', ['meta' => $product->getMeta()])
                </div>
                <!-- /.tab-pane -->
                @if($mode != 'create')
                    <div class="tab-pane" id="tab_files">

                        @include('admin.files._list', ['files' => $product->getFiles(),
                        'owner_id' => $product->id,
                        'owner_class' => get_class($product)
                        ])

                    </div>
                @endif
                <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
        </div>
        <!-- nav-tabs-custom -->

    </div>
    <!-- /.box-body -->

    <div class="box-footer">
        @include('admin.page_elements._buttons', ['mode' => $mode, 'cancel_route' => 'admin.product'])
    </div>
</form>
@include('admin.files._modal_form')