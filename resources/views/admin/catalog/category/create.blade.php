@extends('admin.layouts.onecolumn')

@section('header', __('admin.categs.TTL_NEW_CATEGORY'))
@section('breadcrumbs', Breadcrumbs::render())
@section('main_block_content')

    <div class="row">
        <div class="col-xs-12">
            <div class="box">


                @include('admin.catalog.category._form', [
                'category' => $category,
                'mode' => 'create',
                'tree' => $tree,
                'action' => route('admin.category.store')])

            </div>
        </div>
    </div>

@endsection
