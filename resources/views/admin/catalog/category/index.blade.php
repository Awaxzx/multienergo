@extends('admin.layouts.onecolumn')

@section('header', __('admin.categs.TTL_CATEGORIES'))
@section('breadcrumbs', Breadcrumbs::render())
@section('main_block_content')

    <div class="row">
        <div class="col-xs-12">
            <div class="box">
                <div class="box-header">
                    <div class="text-right"><a href="{{route('admin.category.create')}}" class="btn btn-primary"><span
                                class="fa fa-plus"></span> {{__('admin.categs.TTL_ADD_NEW_CATEGORY')}}</a></div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <table id="table1" class="table table-bordered table-striped">
                        <thead>
                        <tr>
                            <th>ID</th>
                            <th>{{__('admin.categs.ATR_NAME')}}</th>
                            <th>{{__('admin.categs.ATR_URI')}}</th>
                            <th>{{__('admin.categs.ATR_IMAGE')}}</th>
                            <th>{{__('admin.ATR_SORT')}}</th>
                            <th>{{__('admin.ATR_STATUS')}}</th>
                            <th>{{__('admin.ATR_CREATED_AT')}}</th>
                            <th>{{__('admin.ATR_UPDATED_AT')}}</th>
                            <th>{{__('admin.ATR_ACTIONS')}}</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($categories as $category)
                            <tr>
                                <td>{{$category->id}}</td>
                                <td>{{$category->name}}</td>
                                <td>{{$category->fulluri}}</td>
                                <td>{!! ($category->image)?'<a class="fancybox" href="'.asset('storage/'.$category->getImage()).'"><img src="'.asset('storage/'.$category->getImage()).'" style="width:10rem" /></a>':''!!}</td>
                                <td>{{$category->sort}}</td>
                                <td>{{$category->status}}</td>
                                <td>{{$category->created_at}}</td>
                                <td>{{$category->updated_at}}</td>
                                <td>
                                    <a href="{{route('admin.category.show', $category->id)}}"
                                       class="btn btn-xs btn-info"><span
                                            class="fa fa-eye"></span> {{__('admin.TTL_VIEW')}}</a>
                                    <a href="{{route('admin.category.edit', $category->id)}}"
                                       class="btn btn-xs btn-success"><span
                                            class="fa fa-edit"></span> {{__('admin.TTL_EDIT')}}</a>
                                    <form method="POST" action="{{ route('admin.category.destroy', $category) }}"
                                          class="inline">
                                        @csrf
                                        @method('DELETE')
                                        <button class="btn btn-xs btn-danger" onclick="return confirm('{{__('admin.TTL_DELETE_SURE')}}')"><span
                                                class="fa fa-remove"></span> {{__('admin.TTL_DELETE')}}
                                        </button>
                                    </form>

                                </td>
                            </tr>
                        @endforeach

                        </tbody>
                        <tfoot>
                        <tr>
                            <th>ID</th>
                            <th>{{__('admin.categs.ATR_NAME')}}</th>
                            <th>{{__('admin.categs.ATR_URI')}}</th>
                            <th>{{__('admin.categs.ATR_IMAGE')}}</th>
                            <th>{{__('admin.ATR_SORT')}}</th>
                            <th>{{__('admin.ATR_STATUS')}}</th>
                            <th>{{__('admin.ATR_CREATED_AT')}}</th>
                            <th>{{__('admin.ATR_UPDATED_AT')}}</th>
                            <th>{{__('admin.ATR_ACTIONS')}}</th>
                        </tr>
                        </tfoot>
                    </table>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
        </div>
    </div>

@endsection
