<form action="{{$action}}" method="post" enctype="multipart/form-data">
    @csrf
    @if(isset($method) && $method == 'put')
        @method('PUT')
    @endif
    <div class="box-body">

    @include('admin.page_elements._errors_list')

    <!-- Custom Tabs -->
        <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
                <li class="active"><a href="#tab_category" data-toggle="tab">{{__('admin.categs.TTL_CATEGORY')}}</a></li>
                <li><a href="#tab_meta" data-toggle="tab">{{__('admin.meta.TTL_META')}}</a></li>
                @if($mode != 'create')
                    <li><a href="#tab_files" data-toggle="tab">{{__('admin.files.TTL_FILES')}}</a></li>
                @endif
            </ul>
            <div class="tab-content">
                <div class="tab-pane active" id="tab_category">

                    <div class="form-group{{ $errors->has('name') ? ' has-error' : ' has-success' }}">
                        <label for="name">{{__('admin.categs.ATR_NAME')}}</label>
                        <input type="text" id="name" name="name" value="{{ old('name', $category->name) }}" class="form-control" placeholder="Enter Category name" />
                        @if ($errors->has('name'))
                            <span class="help-block"><strong>{{ $errors->first('name') }}</strong></span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->has('parent_id') ? ' has-error' : ' has-success' }}">
                        <label for="parent_id">{{__('admin.categs.ATR_PARENT_CATEGORY')}}</label>
                        <select class="form-control" name="parent_id" id="parent_id">
                            <option value="0" @if($category->parent_id == 0) selected @endif>/</option>
                            @foreach ($tree as $category_id => $categ)
                                <option value="{{ $category_id }}" @if($category->parent_id == $category_id) selected @endif>{{ $categ['offset']}}{{ $categ['name']}}</option>
                            @endforeach
                        </select>

                        @if ($errors->has('parent_id'))
                            <span class="help-block"><strong>{{ $errors->first('parent_id') }}</strong></span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->has('uri') ? ' has-error' : ' has-success' }}">
                        <label for="name">{{__('admin.categs.ATR_URI')}}</label>
                        <input type="text" id="uri" name="uri" value="{{ old('uri', $category->uri) }}" class="form-control" placeholder="Enter Category URI" />
                        @if ($errors->has('uri'))
                            <span class="help-block"><strong>{{ $errors->first('uri') }}</strong></span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->has('content') ? ' has-error' : ' has-success' }}">
                        <label for="name">{{__('admin.categs.ATR_CONTENT')}}</label>
                        <textarea id="content" name="content" class="form-control htmleditor" placeholder="Enter Category content" >{{ old('content', $category->content) }}</textarea>
                        @if ($errors->has('content'))
                            <span class="help-block"><strong>{{ $errors->first('content') }}</strong></span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->has('image') ? ' has-error' : ' has-success' }}">
                        <label for="name">{{__('admin.categs.ATR_IMAGE')}}</label>
                        @isset($category->image)
                            <img src="{{ asset('storage/'.$category->getImage())}}" alt="">
                        @endisset
                        <input type="file" id="image" name="image" value="{{ old('image', $category->image) }}" />
                        @if ($errors->has('image'))
                            <span class="help-block"><strong>{{ $errors->first('image') }}</strong></span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->has('sort') ? ' has-error' : ' has-success' }}">
                        <label for="sort">{{__('admin.ATR_SORT')}}</label>
                        <input type="text" class="form-control" name="sort" id="sort" value="{{ old('sort', $category->sort) }}" placeholder="Enter sort">
                        @if ($errors->has('sort'))
                            <span class="help-block"><strong>{{ $errors->first('sort') }}</strong></span>
                        @endif
                    </div>
                    <div class="form-group{{ $errors->has('status') ? ' has-error' : ' has-success' }}">
                        <label for="status">{{__('admin.ATR_STATUS')}}</label>
                        <select class="form-control" name="status" id="status">
                            @foreach (\App\Models\Category::statuses() as $status)
                                <option value="{{ $status }}" @if($status == $category->status) selected @endif>{{ __("status.$status") }}</option>
                            @endforeach;
                        </select>

                        @if ($errors->has('status'))
                            <span class="help-block"><strong>{{ $errors->first('status') }}</strong></span>
                        @endif
                    </div>



                </div>
                <!-- /.tab-pane -->
                <div class="tab-pane" id="tab_meta">
                    @include('admin.meta._form', ['meta' => $category->getMeta()])
                </div>
                <!-- /.tab-pane -->
                @if($mode != 'create')
                    <div class="tab-pane" id="tab_files">

                        @include('admin.files._list', ['files' => $category->getFiles(),
                        'owner_id' => $category->id,
                        'owner_class' => get_class($category)
                        ])

                    </div>
                @endif
                <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
        </div>
        <!-- nav-tabs-custom -->

    </div>
    <!-- /.box-body -->

    <div class="box-footer">

        @include('admin.page_elements._buttons', ['mode' => $mode, 'cancel_route' => 'admin.category'])

    </div>
</form>
@include('admin.files._modal_form')