@extends('admin.layouts.onecolumn')

@section('header', __('admin.categs.TTL_EDIT_CATEGORY'))
@section('breadcrumbs', Breadcrumbs::render())
@section('main_block_content')
    <!-- Main content -->

    <div class="row">
        <div class="col-xs-12">
            <div class="box">


                @include('admin.catalog.category._form', [
                'method' => 'put',
                'mode' => 'update',
                'category' => $category,
                'tree' => $tree,
                'action' => route('admin.category.update', ['category' => $category])
                ])

            </div>
        </div>
    </div>

@endsection
