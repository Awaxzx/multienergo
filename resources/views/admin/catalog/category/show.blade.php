@extends('admin.layouts.onecolumn')

@section('header', __('admin.categs.TTL_VIEW_CATEGORY'))
@section('breadcrumbs', Breadcrumbs::render())
@section('main_block_content')
    <div class="row">
        <div class="col-xs-12">
            <div class="box">

                <div class="box-body">
                    <div class="text-right">
                        <a href="{{route('admin.category')}}" class="btn btn-info"><span
                                class="fa fa-list"></span> {{__('admin.TTL_BACK_TO_LIST')}}</a>
                        <a href="{{route('admin.category.edit', $category->id)}}" class="btn btn-success"><span
                                class="fa fa-edit"></span> {{__('admin.TTL_EDIT')}}</a>
                        <form method="POST" action="{{ route('admin.category.destroy', $category) }}" class="inline">
                            @csrf
                            @method('DELETE')
                            <button class="btn btn-danger" onclick="return confirm('{{__('admin.TTL_DELETE_SURE')}}')"><span
                                    class="fa fa-remove"></span> {{__('admin.TTL_DELETE')}}
                            </button>
                        </form>
                        <a href="{{route('admin.category.create')}}" class="btn btn-primary"><span
                                class="fa fa-plus"></span> {{__('admin.categs.TTL_ADD_NEW_CATEGORY')}}</a>
                    </div>
                    <!-- Main content -->
                    <dl class="dl-horizontal">
                        <dt>ID</dt>
                        <dd>{{$category->id}}</dd>

                        <dt>{{__('admin.categs.ATR_NAME')}}</dt>
                        <dd>{{$category->name}}</dd>

                        <dt>{{__('admin.categs.ATR_URI')}}</dt>
                        <dd>{{$category->uri}}</dd>

                        <dt>{{__('admin.categs.ATR_CONTENT')}}</dt>
                        <dd>{{$category->content}}</dd>

                        <dt>{{__('admin.categs.ATR_IMAGE')}}</dt>
                        <dd>
                            @isset($category->image)
                                <img src="{{ asset('storage/'.$category->getImage())}}" alt="">
                            @endisset
                        </dd>

                        <dt>{{__('admin.ATR_SORT')}}</dt>
                        <dd>{{$category->sort}}</dd>

                        <dt>{{__('admin.ATR_STATUS')}}</dt>
                        <dd>{{$category->status}}</dd>
                    </dl>

                    @include('admin.meta._show_meta', ['item' => $category])
                    @include('admin.files._show_files', ['files' => $category->getFiles()])
                </div>
            </div>
        </div>
    </div>
@endsection
