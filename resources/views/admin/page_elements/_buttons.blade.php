<div class="text-right">
    <a href="{{route($cancel_route)}}" class="btn btn-default">{{__('admin.BTN_CANCEL')}}</a>
    @if($mode == 'create')
        <input type="submit" name="CREATE" class="btn btn-primary" value="{{__('admin.BTN_CREATE')}}">
        <input type="submit" name="CREATE_AND_VIEW" class="btn btn-primary" value="{{__('admin.BTN_CREATE_AND_VIEW')}}">
        <input type="submit" name="CREATE_AND_CREATE" class="btn btn-primary"
               value="{{__('admin.BTN_CREATE_AND_CREATE')}}">
        <input type="submit" name="CREATE_AND_UPDATE" class="btn btn-primary"
               value="{{__('admin.BTN_CREATE_AND_UPDATE')}}">
    @elseif($mode == 'update')
        <input type="submit" name="UPDATE" class="btn btn-primary" value="{{__('admin.BTN_UPDATE')}}">
        <input type="submit" name="UPDATE_AND_VIEW" class="btn btn-primary" value="{{__('admin.BTN_UPDATE_AND_VIEW')}}">
        <input type="submit" name="UPDATE_AND_CONTINUE" class="btn btn-primary"
               value="{{__('admin.BTN_UPDATE_AND_CONTINUE')}}">
        <input type="submit" name="UPDATE_AND_CREATE" class="btn btn-primary"
               value="{{__('admin.BTN_UPDATE_AND_CREATE')}}">
    @endif
</div>