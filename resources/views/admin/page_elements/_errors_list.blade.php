@if($errors->all())
<div class="callout callout-danger">
    <h4><i class="icon fa fa-warning"></i> Errors!</h4>
    <ul>
        @foreach ($errors->all() as $error)
            <li class="error">{{ $error }}</li>
        @endforeach
    </ul>
</div>
@endif