@extends('layouts.app')

@section('content')
    <article class="g-mt-20 g-mb-100">
        {!! $page->content !!}
    </article>
@endsection
