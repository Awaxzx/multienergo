@extends('layouts.app')

@section('content')
    <article class="g-mt-20 g-mb-100">
        {!! $page->content !!}


        <div class="container">
            @foreach($files as $file)
                <section>
                    <header class="g-mb-15">
                        <h2 class="h4 g-mb-5">
                            <a class="u-link-v5 g-color-gray-dark-v1 g-color-primary--hover" href="{{asset('storage/'.$file->getFile())}}">{!! $file->title !!}</a>
                        </h2>
                    </header>
                    @if($file->description)
                        <p class="g-mb-15">{!! $file->description !!}</p>
                    @endif
                    <a href="{{asset('storage/'.$file->getFile())}}"><span class="g-color-primary">{{asset('storage/'.$file->getFile())}}</span></a>

                </section>

                <hr class="g-brd-gray-light-v4 g-my-20">
            @endforeach
        </div>


    </article>

@endsection
