@extends('layouts.app')

@section('content')
    <article class="g-mt-20 g-mb-100">

        <h3>{{$page->name}}</h3>

        <!-- Google Map -->
        <div class="js-g-map embed-responsive embed-responsive-21by9 g-height-400 g-mt-20">

            <iframe width="100%" height="100%" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"
                    src="https://maps.google.com/maps?q=47.037873,28.834382&amp;num=1&amp;t=m&amp;ie=UTF8&amp;ll=47.037873,28.834382&amp;spn=0.006252,0.016512&amp;z=16&amp;output=embed"></iframe>


        </div>
        <!-- End Google Map -->

        <section class="container g-pt-100 g-pb-40">
            <div class="row justify-content-between">
                <div class="col-md-7 g-mb-60">

                    @if (Session::has('flash_message'))
                        <div class="alert alert-success">{{Session::get('flash_message')}}</div>
                    @endif

                <!-- Contact Form -->
                    <form action="{{ route('contacts.send') }}" method="post">
                        @csrf
                        <div class="row">
                            <div class="col-md-6 form-group g-mb-20">
                                <label class="g-color-gray-dark-v2 g-font-size-13">{{__('site.FORM_NAME')}}: <span
                                            class="g-color-red">*</span></label>
                                <input class="form-control g-color-gray-dark-v5 g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-brd-primary--focus rounded-3 g-py-13 g-px-15"
                                       name="name"
                                       type="text"
                                       value="{{ old('name') }}">
                                @if ($errors->has('name'))
                                    <span class="help-block g-color-red"><strong>{{ $errors->first('name') }}</strong></span>
                                @endif
                            </div>

                            <div class="col-md-6 form-group g-mb-20">
                                <label class="g-color-gray-dark-v2 g-font-size-13">{{__('site.FORM_EMAIL')}}: <span
                                            class="g-color-red">*</span></label>
                                <input class="form-control g-color-gray-dark-v5 g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-brd-primary--focus rounded-3 g-py-13 g-px-15"
                                       name="email"
                                       type="email"
                                       value="{{ old('email') }}">
                                @if ($errors->has('email'))
                                    <span class="help-block g-color-red"><strong>{{ $errors->first('email') }}</strong></span>
                                @endif
                            </div>

                            <div class="col-md-6 form-group g-mb-20">
                                <label class="g-color-gray-dark-v2 g-font-size-13">{{__('site.FORM_SUBJECT')}}:</label>
                                <input class="form-control g-color-gray-dark-v5 g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-brd-primary--focus rounded-3 g-py-13 g-px-15"
                                       name="subject"
                                       type="text"
                                       value="{{ old('subject') }}">
                            </div>

                            <div class="col-md-6 form-group g-mb-20">
                                <label class="g-color-gray-dark-v2 g-font-size-13">{{__('site.FORM_PHONE')}}:</label>
                                <input class="form-control g-color-gray-dark-v5 g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-brd-primary--focus rounded-3 g-py-13 g-px-15"
                                       name="phone"
                                       type="tel"
                                       value="{{ old('phone') }}">
                            </div>

                            <div class="col-md-12 form-group g-mb-40">
                                <label class="g-color-gray-dark-v2 g-font-size-13">{{__('site.FORM_MESSAGE')}}: <span
                                            class="g-color-red">*</span></label>
                                <textarea
                                        class="form-control g-color-gray-dark-v5 g-bg-white g-bg-white--focus g-brd-gray-light-v4 g-brd-primary--focus g-resize-none rounded-3 g-py-13 g-px-15"
                                        rows="7"
                                        name="message">{{ old('message') }}</textarea>
                                @if ($errors->has('message'))
                                    <span class="help-block g-color-red"><strong>{{ $errors->first('message') }}</strong></span>
                                @endif
                            </div>
                        </div>

                        <button class="btn u-btn-primary rounded-3 g-py-12 g-px-20" type="submit"
                                role="button">{{__('site.FORM_SEND')}}
                        </button>
                    </form>
                    <!-- End Contact Form -->
                </div>

                <div class="col-md-4">

                    {!! $page->content !!}

                </div>
            </div>
        </section>
    </article>
@endsection

