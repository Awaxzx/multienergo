@extends('layouts.app')

@section('content')
    <article class="g-mt-20 g-mb-100">
        {!! $page->content !!}


        <div class="row text-center">
            @foreach($certificates as $certificate)
                <div class="col-md-4 g-mb-30">
                    <a class="js-fancybox d-block u-block-hover u-bg-overlay u-bg-overlay--v1 g-bg-black-opacity-0_5--after"
                       href="javascript:;" data-fancybox="lightbox-gallery-13-1-hidden"
                       data-src="{{asset('storage/'.$certificate->getImage())}}" data-caption="Lightbox Gallery">
                        <img class="img-fluid u-block-hover__main--zoom-v1" src="{{asset('storage/'.$certificate->getThumbnail())}}"
                             alt="{{$certificate->name}}">
                        <strong class="h5 d-block w-100 u-bg-overlay__inner g-absolute-centered g-px-20 g-font-weight-400 g-color-white g-mt-10">{{$certificate->name}}</strong>
                    </a>
                </div>
            @endforeach
        </div>


    </article>

@endsection
