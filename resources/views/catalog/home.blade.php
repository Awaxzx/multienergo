@extends('layouts.app')

@section('content')
    <article class="g-mt-20 g-mb-50">
        {!! $page->content !!}
    </article>

    @include('catalog._category', ['categories' => $categories])

@endsection