@extends('layouts.app')

@section('content')

    @include('catalog._category', ['categories' => $categories, 'currentCategoryId' => $currentCategoryId])

@endsection