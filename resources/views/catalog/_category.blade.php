    <section class="g-mt-20 g-mb-100">
        @foreach($categories as $category)
            {{--@extends('catalog.category', ['currentCategoryId' => $category['id']])--}}
        <?php
            view()->share('currentCategoryId', $currentCategoryId);
            ?>
            <div class="g-mb-30">
                <h2 class="h2 g-color-black mb-3">
                    @if($currentCategoryId == $category['id'])
                        {{$category['name']}}
                    @else
                    <a class="u-link-v5 g-color-black g-color-primary--hover" href="{{ route('catalog.category', ['fulluri' => $category['fulluri']]) }}">{{$category['name']}}</a>
                    @endif
                </h2>

                <div class="g-color-gray-dark-v4 g-line-height-1_8">
                    @if($category['image'])
                        <div class="text-center"><img src="{{asset('storage/'.$category['image'])}}" class="img-fluid" alt="{{$category['name']}}" /></div>
                    @endif
                    {!! $category['content'] !!}
                </div>
                @if($category['has_files'])
                    @include('catalog._files', ['files' => $category['files']])
                @endif
                @if($category['has_children'])
                    @include('catalog._category_list', ['categories' => $category['children']])
                @endif

                @if($category['has_products'])
                    @include('catalog._product_list', ['products' => $category['products']])
                @endif

            </div>
        @endforeach
    </section>