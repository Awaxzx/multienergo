<div class="g-pl-50">
@foreach($categories as $category)
    <article>
        <h3 class="h4 g-mb-15">
            <a class="u-link-v5 g-color-gray-dark-v1 g-color-primary--hover" href="{{route('catalog.category', ['fulluri' => $category['fulluri']])}}">{{$category['name']}}</a>
        </h3>

        <p class="g-mb-10">{{ strip_tags($category['content'], 'br') }}</p>


    </article>
    <!-- End Search Result -->

    <hr class="g-brd-gray-light-v4 g-my-20">
@endforeach
</div>