<!-- Products -->
<div class="row g-pt-30 g-mb-50">

    @foreach($products as $product)

        <div class="col-6 col-lg-4 g-mb-30">
            <!-- Product -->
            <figure class="g-pos-rel g-mb-20">
                <a class=""
                   href="{{route('catalog.product', ['uri' => $product['uri']])}}">
                    <img class="img-fluid" src="{{asset('storage/'.$product['thumbnail'])}}">
                </a>
            </figure>

            <div class="media">
                <!-- Product Info -->
                <div class="d-flex flex-column">
                    <h4 class="h6 g-color-black mb-1">
                        <a class="u-link-v5 g-color-black g-color-primary--hover"
                           href="{{route('catalog.product', ['uri' => $product['uri']])}}">
                            {{$product['name']}}
                        </a>
                    </h4>
                </div>
                <!-- End Product Info -->


            </div>
            <!-- End Product -->
        </div>
    @endforeach
</div>
