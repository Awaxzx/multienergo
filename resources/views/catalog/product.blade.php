@extends('layouts.app')

@section('content')
    <?php
    view()->share('currentCategoryId', $product->category_id);
    ?>

    <article class="g-mt-20 g-mb-50 row">
        <div class="col-md-5">
            @if($product->image)
                <a class="js-fancybox"
                   href="javascript:;" data-fancybox="lightbox-gallery-13-1-hidden"
                   data-src="{{asset('storage/'.$product->getImage())}}" data-caption="{{$product->name}}">
                    <img class="img-fluid u-block-hover__main--zoom-v1" src="{{asset('storage/'.$product->getThumbnail())}}"
                         alt="{{$product->name}}">
                </a>

            @endif    
        </div>
        <div class="col-md-7">
            <h2>{!! $product->name !!}</h2>
            {!! $product->short_content !!}
        </div>
        <div class="col">{!! $product->content !!}</div>
        @include('catalog._files', ['files' => $product->getFiles()])
    </article>
@endsection