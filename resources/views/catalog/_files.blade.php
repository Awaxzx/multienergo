@if(count($files) >0 )
<div class="container">
    <h2 class="g-mt-15">Файлы</h2>
    <hr class="g-brd-gray-light-v4 g-my-10">
    @foreach($files as $file)
        <section>
            <header class="g-mb-15">
                <h2 class="h4 g-mb-5">
                    <a class="u-link-v5 g-color-gray-dark-v1 g-color-primary--hover" href="{{asset('storage/'.$file->getFile())}}">{!! $file->title !!}</a>
                </h2>
            </header>
            @if($file->description)
                <p class="g-mb-15">{!! $file->description !!}</p>
            @endif
            <a href="{{asset('storage/'.$file->getFile())}}"><span class="g-color-primary">{{asset('storage/'.$file->getFile())}}</span></a>
            {{--<div class="d-lg-flex justify-content-between align-items-center">

                <ul class="list-inline g-mb-10 g-mb-0--lg">
                    <li class="list-inline-item g-mr-30">
                        <i class="icon-arrow-down-circle g-pos-rel g-top-1 g-color-gray-dark-v5 g-mr-5"></i> {{$file->getSize()}}
                    </li>
                    <li class="list-inline-item g-mr-30">
                        <i class="icon-eye g-pos-rel g-top-1 g-color-gray-dark-v5 g-mr-5"></i> {{$file->type}}
                    </li>
                </ul>


            </div>--}}
        </section>


        <hr class="g-brd-gray-light-v4 g-my-20">
    @endforeach
</div>
@endif