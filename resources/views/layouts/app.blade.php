<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>

<?php if (!isset($meta_title)) $meta_title = '';?>
<?php if (!isset($meta_keywords)) $meta_keywords = env('APP_KEYWORDS');?>
<?php if (!isset($meta_description)) $meta_description = env('APP_DESCRIPTION');?>

<!-- Title -->
    <title>{{$meta_title}} @if(!empty($meta_title))- @endif{{ config('app.name', 'MultiEnergo') }}</title>

    <!-- Required Meta Tags Always Come First -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="keywords" content="{{$meta_keywords}}">
    <meta name="description" content="{{$meta_description}}">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Favicon -->
    <link rel="shortcut icon" href="favicon.ico">
    <!-- Google Fonts -->
    <link rel="stylesheet"
          href="//fonts.googleapis.com/css?family=Open+Sans%3A400%2C300%2C500%2C600%2C700%7CPlayfair+Display%7CRoboto%7CRaleway%7CSpectral%7CRubik">
    <link rel="stylesheet" href="//fonts.googleapis.com/css?family=Open+Sans%3A400%2C300%2C500%2C600%2C700">
    <link href="{{ mix('css/app.css', 'build') }}" rel="stylesheet">


</head>

<body>

<main>
    <!-- Header -->
    <header id="js-header" class="u-header u-header--static u-shadow-v19">
        <!-- Top Bar -->
        <div class="u-header__section g-brd-bottom g-brd-gray-light-v4 g-bg-black g-transition-0_3">
            <div class="container">
                <div class="row flex-column flex-md-row align-items-center justify-content-between g-color-white g-mx-0--lg  g-py-14">
                    <div class="row col-auto g-pos-rel g-px-15 mb-0">
                        <div>
                            <i class="icon-phone g-font-size-24 g-valign-middle g-color-primary g-mr-10  g-pt-15"></i>
                        </div>
                        <div class="g-color-white-opacity-0_8">{!!__('site.TXT_PHONES')!!}</div>
                    </div>

                    <div class="row col-auto g-pos-rel g-px-15 mb-0 g-color-white-opacity-0_8">
                        <div>
                            <i class="icon-location-pin g-font-size-24 g-valign-middle g-color-primary g-mr-10  g-pt-15"></i>
                        </div>
                        <div class="g-color-white-opacity-0_8">
                            {!!__('site.TXT_ADDRESS')!!}
                        </div>
                    </div>

                    <div class="col-auto g-px-15 g-color-white-opacity-0_8">
                        <i class="icon-envelope g-font-size-24 g-valign-middle g-color-primary g-mr-15"></i>
                        <a href="mailto:{!!__('site.TXT_EMAIL')!!}"
                           class="g-color-white g-color-primary--hover g-text-underline--none--hover">{!!__('site.TXT_EMAIL')!!}</a>
                    </div>

                </div>
            </div>
        </div>
        <!-- End Top Bar -->


        <div class="u-header__section u-header__section--light g-bg-white g-transition-0_3 g-py-10">
            <nav class="navbar navbar-expand-lg">
                <div class="container">
                    <!-- Responsive Toggle Button -->
                    <button class="navbar-toggler navbar-toggler-right btn g-line-height-1 g-brd-none g-pa-0 g-pos-abs g-top-3 g-right-0"
                            type="button" aria-label="Toggle navigation" aria-expanded="false" aria-controls="navBar"
                            data-toggle="collapse" data-target="#navBar">
              <span class="hamburger hamburger--slider">
            <span class="hamburger-box">
              <span class="hamburger-inner"></span>
              </span>
              </span>
                    </button>
                    <!-- End Responsive Toggle Button -->
                    <!-- Logo -->
                    <a href="/" class="navbar-brand">
                        <img src="/images/Logo.jpg" alt="MultiEnergo">
                    </a>
                    <!-- End Logo -->

                    <!-- Navigation -->
                    @widget('menu', ['type' => 'top', 'template' => 'top'])

                    <!-- End Navigation -->
                </div>
            </nav>
        </div>
    </header>
    <!-- End Header -->


    <div class="container">
        <div class="row">
            <!-- Content -->
            <div class="col-md-9 order-2">
                <div class="g-pl-15--lg">


                    @yield('content')


                </div>
            </div>
            <!-- End Content -->

            <!-- Sidebar -->
            <div class="col-md-3 order-1 g-brd-right--lg g-brd-gray-light-v4 g-mt-20">
                <div class="g-pr-15--lg">
                    <!-- Categories -->
                    <div class="g-mb-30">
                        <h3 class="">{{__('site.TTL_CATALOG')}}</h3>

                        <?php
                        if (!isset($currentCategoryId)) $currentCategoryId = 0;
                        ?>

                        @widget('CatalogMenu', ['currentCategoryId' => $currentCategoryId])


                    </div>
                    <!-- End Categories -->

                    <hr>


                </div>
            </div>
            <!-- End Sidebar -->
        </div>
    </div>


    <!-- Footer -->
    <div id="contacts-section" class="g-bg-black-opacity-0_9 g-color-white-opacity-0_8 g-py-60">
        <div class="container">
            <div class="row">
                <!-- Footer Content -->
                <div class="col-lg-3 col-md-6 g-mb-40 g-mb-0--lg">
                    <div class="u-heading-v2-3--bottom g-brd-white-opacity-0_8 g-mb-20">
                        <h2 class="u-heading-v2__title h6 text-uppercase mb-0">{{__('site.TTL_ABOUT')}}</h2>
                    </div>

                    <p>{!!__('site.TXT_ABOUT')!!}.</p>
                </div>
                <!-- End Footer Content -->

                <!-- Footer Content -->
                <div class="col-lg-3 col-md-6 g-mb-40 g-mb-0--lg">
                    <div class="u-heading-v2-3--bottom g-brd-white-opacity-0_8 g-mb-20">
                        <h2 class="u-heading-v2__title h6 text-uppercase mb-0">{{__('site.TTL_CATALOG')}}</h2>
                    </div>
                    @widget('CatalogMenu', ['depth' => 1, 'template' => 'bottom'])

                </div>
                <!-- End Footer Content -->

                <!-- Footer Content -->
                <div class="col-lg-3 col-md-6 g-mb-40 g-mb-0--lg">
                    <div class="u-heading-v2-3--bottom g-brd-white-opacity-0_8 g-mb-20">
                        <h2 class="u-heading-v2__title h6 text-uppercase mb-0">{{__('site.TTL_MENU')}}</h2>
                    </div>

                    <nav class="text-uppercase1">
                        @widget('menu', ['type' => 'bottom', 'template' => 'bottom'])

                    </nav>
                </div>
                <!-- End Footer Content -->

                <!-- Footer Content -->
                <div class="col-lg-3 col-md-6">
                    <div class="u-heading-v2-3--bottom g-brd-white-opacity-0_8 g-mb-20">
                        <h2 class="u-heading-v2__title h6 text-uppercase mb-0">{{__('site.TTL_CONTACTS')}}</h2>
                    </div>

                    <address
                            class="g-bg-no-repeat g-font-size-12 mb-0"
                            style="background-image: url(/assets/img/maps/map2.png);">
                        <!-- Location -->
                        <div class="d-flex g-mb-20">
                            <div class="g-mr-10">
              <span class="u-icon-v3 u-icon-size--xs g-bg-white-opacity-0_1 g-color-white-opacity-0_6">
                <i class="fa fa-map-marker"></i>
              </span>
                            </div>
                            <p class="mb-0">{!!__('site.TXT_ADDRESS')!!}</p>
                        </div>
                        <!-- End Location -->

                        <!-- Phone -->
                        <div class="d-flex g-mb-20">
                            <div class="g-mr-10">
              <span class="u-icon-v3 u-icon-size--xs g-bg-white-opacity-0_1 g-color-white-opacity-0_6">
                <i class="fa fa-phone"></i>
              </span>
                            </div>
                            <p class="mb-0">{!!__('site.TXT_PHONES')!!}</p>
                        </div>
                        <!-- End Phone -->

                        <!-- Email and Website -->
                        <div class="d-flex g-mb-20">
                            <div class="g-mr-10">
              <span class="u-icon-v3 u-icon-size--xs g-bg-white-opacity-0_1 g-color-white-opacity-0_6">
                <i class="fa fa-globe"></i>
              </span>
                            </div>
                            <p class="mb-0 g-pt-5">
                                <a class="g-color-white-opacity-0_8 g-color-white--hover"
                                   href="mailto:{!!__('site.TXT_EMAIL')!!}">{!!__('site.TXT_EMAIL')!!}</a>
                            </p>
                        </div>
                        <!-- End Email and Website -->
                    </address>
                </div>
                <!-- End Footer Content -->
            </div>
        </div>
    </div>
    <!-- End Footer -->

    <!-- Copyright Footer -->
    <footer class="g-bg-gray-dark-v1 g-color-white-opacity-0_8 g-py-20">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center text-md-left g-mb-10 g-mb-0--md">
                    <div class="d-lg-flex">
                        <small class="d-block g-font-size-default g-mr-10 g-mb-10 g-mb-0--md">{{date('Y')}} &copy; All
                            Rights Reserved.
                        </small>

                    </div>
                </div>
            </div>
        </div>
    </footer>
    <!-- End Copyright Footer -->

</main>

<!-- JS Global Compulsory -->

{{--<script src="assets/vendor/jquery-migrate/jquery-migrate.min.js"></script>--}}


<script src="{{ mix('js/app.js', 'build') }}" defer></script>
</body>
</html>

