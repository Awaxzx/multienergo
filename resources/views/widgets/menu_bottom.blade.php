<ul class="list-unstyled g-mt-minus-10 mb-0">
    @foreach($pages as $page)
        <li class="g-pos-rel @if(!$loop->last) g-brd-bottom @endif g-brd-white-opacity-0_1 g-py-10">
            <h4 class="h6 g-pr-20 mb-0">

                @if($page->uri == '/')
                    <a href="{{ route('home') }}" class="g-color-white-opacity-0_8 g-color-white--hover">{{$page->name}}</a>
                @else
                    <a href="{{ route('pages.page', ['uri' => $page->fulluri]) }}" class="g-color-white-opacity-0_8 g-color-white--hover">{{$page->name}}</a>@endif

                <i class="fa fa-angle-right g-absolute-centered--y g-right-0"></i>
            </h4>
        </li>

    @endforeach
</ul>