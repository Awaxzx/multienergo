@foreach($tree as $item)
    <article>
        <h3 class="h6 g-mb-2">
            <a class="g-color-white-opacity-0_8 g-color-white--hover"
               href="{{route('catalog.category', ['fulluri' => $item['fulluri']])}}">{{$item['name']}}</a>
        </h3>
    </article>
    @if(!$loop->last)
        <hr class="g-brd-white-opacity-0_1 g-my-10">
    @endif
@endforeach
