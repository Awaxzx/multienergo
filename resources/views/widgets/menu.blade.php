<div class="collapse navbar-collapse align-items-center flex-sm-row g-pt-15 g-pt-0--lg" id="navBar">
    <ul class="navbar-nav text-uppercase g-font-weight-600 ml-auto">
        @foreach($pages as $page)
            @if($page->uri == '/')
                <li class="nav-item g-mx-20--lg{{ (Request::path() == '/')?' active':'' }}"><a href="{{ route('home') }}" class="nav-link px-0">{{$page->name}}</a></li>
            @else
                <li class="nav-item g-mx-20--lg{{ (Request::path() == $page->fulluri.".html")?' active':'' }}"><a href="{{ route('pages.page', ['uri' => $page->fulluri]) }}" class="nav-link px-0">{{$page->name}}</a></li>
            @endif
        @endforeach
    </ul>
</div>