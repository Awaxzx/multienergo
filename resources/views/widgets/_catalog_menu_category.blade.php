@foreach($tree as $item)
    <li class="d-block{{($item['open'])?' active':''}}">
        <span class="d-flex flex-row">
            <a class="p-2 u-link-v5 g-color-gray-dark-v4 rounded g-py-8"
               href="{{route('catalog.category', ['fulluri' => $item['fulluri']])}}">{{$item['name']}}
            </a>
            @if($item['has_children'])
                <a class="p-1 u-link-v5 g-color-gray-dark-v4 g-px-20 g-py-8 bold" href="#"><i
                            class="fa fa-angle-down"></i>
                </a>
            @endif
        </span>

        @if($item['has_children'])
            <ul class="list-unstyled mb-0" style="display:{{($item['open'])?'block':'none'}}">
                @include('widgets._catalog_menu_category', ['tree' => $item['children']])
            </ul>
        @endif

    </li>

@endforeach