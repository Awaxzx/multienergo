<?php

return [
    // Меню
    'MENU_DASHBOARD'          => 'Главная',
    'MENU_USERS'              => 'Пользователи',
    'MENU_PAGES'              => 'Страницы',
    'MENU_CATEGORIES'         => 'Категории',
    'MENU_PRODUCTS'           => 'Товары',
    'MENU_CERTIFICATES'       => 'Сертификаты',

    // Общие заголовки
    'TTL_SORT'                => 'Сортировка',
    'TTL_STATUS'              => 'Статус',
    'TTL_CREATED_AT'          => 'Создано',
    'TTL_UPDATED_AT'          => 'Изменено',
    'TTL_ACTIONS'             => 'Действия',
    'TTL_EDIT'                => 'Редактировать',
    'TTL_VIEW'                => 'Просмотреть',
    'TTL_DELETE'              => 'Удалить',
    'TTL_DELETE_SURE'         => 'Удалить? Вы уверены?',
    'TTL_META_TAGS'           => 'Мета-теги',
    'TTL_FILES'               => 'Файлы',
    'TTL_BACK_TO_LIST'        => 'Вернуться к списку',
    'TTL_CLOSE'               => 'Закрыть',

    // Кнопки
    'BTN_CREATE'              => 'Добавить',
    'BTN_CREATE_AND_VIEW'     => 'Добавить и посмотреть',
    'BTN_CREATE_AND_CREATE'   => 'Добавить и создать еще',
    'BTN_CREATE_AND_UPDATE'   => 'Добавить и редактировать',
    'BTN_UPDATE'              => 'Сохранить',
    'BTN_UPDATE_AND_VIEW'     => 'Сохранить и посмотреть',
    'BTN_UPDATE_AND_CONTINUE' => 'Сохранить и продолжить',
    'BTN_UPDATE_AND_CREATE'   => 'Сохранить и создать еще',
    'BTN_CANCEL'              => 'Отмена',

    // Общие атрибуты
    'ATR_SORT'                => 'Сортировка',
    'ATR_STATUS'              => 'Статус',
    'ATR_FILES'               => 'Файлы',
    'ATR_CREATED_AT'          => 'Создано',
    'ATR_UPDATED_AT'          => 'Изменено',
    'ATR_ACTIONS'             => 'Действия',

    // Сообщения
    'MSG_'                    => '',

    // Мета-теги
    'meta'                    => [
        // Заголовки
        'TTL_META'        => 'Мета-теги',
        // Атрибуты
        'ATR_TITLE'       => 'Мета-тег Title',
        'ATR_KEYWORDS'    => 'Мета-тег Keywords',
        'ATR_DESCRIPTION' => 'Мета-тег Description',
    ],

    // Файлы
    'files'                   => [
        // Заголовки
        'TTL_FILES'         => 'Файлы',
        'TTL_ADD_FILE'      => 'Загрузить файл',
        'TTL_NO_FILES'      => 'Нет файлов',
        'TTL_EDIT_FILE'     => 'Редактирование файла',

        // Сообщения
        'MSG_FILE_UPLOADED' => 'Файл загружен',
        'MSG_FILE_UPDATED'  => 'Изменения сохранены',
        'MSG_FILE_DELETED'  => 'Файл удален',

        // Атрибуты
        'ATR_FILE'          => 'Файл',
        'ATR_FILENAME'      => 'Имя файла',
        'ATR_TYPE'          => 'Тип файла',
        'ATR_TITLE'         => 'Название',
        'ATR_DESCRIPTION'   => 'Описание',
    ],


    // Страницы
    'pages'                   => [
        // Заголовки
        'TTL_PAGES'           => 'Страницы',
        'TTL_ADD_NEW_PAGE'    => 'Создать новую страницу',
        'TTL_NEW_PAGE'        => 'Новая страница',
        'TTL_EDIT_PAGE'       => 'Редактирование страницы',
        'TTL_PAGE'            => 'Страница',
        'TTL_VIEW_PAGE'       => 'Просмотр страницы',

        // Сообщения
        'MSG_PAGE_CREATED'    => 'Страница создана!',
        'MSG_PAGE_UPDATED'    => 'Страница сохранена!',
        'MSG_PAGE_DELETED'    => 'Страница удалена!',

        // Атрибуты
        'ATR_NAME'            => 'Название страницы',
        'ATR_URI'             => 'URI страницы',
        'ATR_PARENT_PAGE'     => 'Родительская страница',
        'ATR_CONTENT'         => 'Контент страницы',
        'ATR_DISPLAY_IN_MENU' => 'Отображать в меню',
    ],


    // Категории
    'categs'                  => [
        // Заголовки
        'TTL_CATEGORIES'       => 'Категории',
        'TTL_ADD_NEW_CATEGORY' => 'Создать новую категорию',
        'TTL_NEW_CATEGORY'     => 'Новая категория',
        'TTL_EDIT_CATEGORY'    => 'Редактирование категории',
        'TTL_CATEGORY'         => 'Категория',
        'TTL_VIEW_CATEGORY'    => 'Просмотр категории',

        // Сообщения
        'MSG_CATEGORY_CREATED' => 'Категория создана!',
        'MSG_CATEGORY_UPDATED' => 'Категория сохранена!',
        'MSG_CATEGORY_DELETED' => 'Категория удалена!',

        // Атрибуты
        'ATR_NAME'             => 'Название категории',
        'ATR_URI'              => 'URI категории',
        'ATR_IMAGE'            => 'Изображение',
        'ATR_PARENT_CATEGORY'  => 'Родительская категория',
        'ATR_CONTENT'          => 'Контент категории',

    ],


    // Товары
    'products'                => [
        // Заголовки
        'TTL_PRODUCTS'        => 'Товары',
        'TTL_ADD_NEW_PRODUCT' => 'Создать новый товар',
        'TTL_NEW_PRODUCT'     => 'Новый товар',
        'TTL_EDIT_PRODUCT'    => 'Редактирование товара',
        'TTL_PRODUCT'         => 'Товар',
        'TTL_VIEW_PRODUCT'    => 'Просмотр товара',

        // Сообщения
        'MSG_PRODUCT_CREATED' => 'Товар создан!',
        'MSG_PRODUCT_UPDATED' => 'Товар сохранен!',
        'MSG_PRODUCT_DELETED' => 'Товар удален!',

        // Атрибуты
        'ATR_NAME'            => 'Название товара',
        'ATR_URI'             => 'URI товара',
        'ATR_THUMBNAIL'       => 'Превью',
        'ATR_IMAGE'           => 'Изображение',
        'ATR_CATEGORY'        => 'Категория товара',
        'ATR_SHORT_CONTENT'   => 'Краткое описание',
        'ATR_CONTENT'         => 'Полное описание',

    ],


    // Сертификаты
    'certificates'            => [
        // Заголовки
        'TTL_CERTIFICATES'        => 'Сертификаты',
        'TTL_ADD_NEW_CERTIFICATE' => 'Создать новый сертификат',
        'TTL_NEW_CERTIFICATE'     => 'Новый сертификат',
        'TTL_EDIT_CERTIFICATE'    => 'Редактирование сертификата',
        'TTL_CERTIFICATE'         => 'Сертификат',
        'TTL_VIEW_CERTIFICATE'    => 'Просмотр сертификата',

        // Сообщения
        'MSG_CERTIFICATE_CREATED' => 'Сертификат создан!',
        'MSG_CERTIFICATE_UPDATED' => 'Сертификат сохранен!',
        'MSG_CERTIFICATE_DELETED' => 'Сертификат удален!',

        // Атрибуты
        'ATR_NAME'                => 'Название сертификата',
        'ATR_THUMBNAIL'           => 'Превью',
        'ATR_IMAGE'               => 'Изображение',
    ],


    // Пользователи
    'users'                   => [
        // Заголовки
        'TTL_USERS'            => 'Пользователи',
        'TTL_ADD_NEW_USER'     => 'Создать нового пользователя',
        'TTL_NEW_USER'         => 'Новый пользователь',
        'TTL_EDIT_USER'        => 'Редактирование пользователя',
        'TTL_USER'             => 'Пользователь',
        'TTL_VIEW_USER'        => 'Просмотр пользователя',

        // Сообщения
        'MSG_USER_CREATED'     => 'Пользователь создан!',
        'MSG_USER_UPDATED'     => 'Пользователь сохранен!',
        'MSG_USER_DELETED'     => 'Пользователь удален!',

        // Атрибуты
        'ATR_NAME'             => 'Имя',
        'ATR_EMAIL'            => 'Email',
        'ATR_PASSWORD'         => 'Пароль',
        'ATR_CONFIRM_PASSWORD' => 'Еще раз пароль',
    ],
];