<?php

return [
    // Заголовки
    'TTL_CATALOG'       => 'Каталог товаров',
    'TTL_ABOUT'         => 'О нас',
    //'TTL_LAST_PRODUCTS' => 'Новинки',
    'TTL_MENU'          => 'Меню',
    'TTL_CONTACTS'      => 'Контакты',

    // Текстовые блоки
    'TXT_ABOUT'         => 'Продажа, установка и обслуживание систем автоматизации для записи потребления природного газа, воды и пара в Молдове',
    'TXT_PHONES'        => '+373 925 414<br>069 454 129',
    'TXT_ADDRESS'       => 'MD 2005 mun. Chişinău<br>str. Ivan Zaikin, 60 c/p 5',
    'TXT_EMAIL'         => 'multienerg@mail.ru',

    // Форма
    'FORM_NAME'         => 'Ваше имя',
    'FORM_PHONE'        => 'Номер телефона',
    'FORM_SUBJECT'      => 'Тема сообщения',
    'FORM_EMAIL'        => 'E-mail',
    'FORM_MESSAGE'      => 'Сообщение',
    'FORM_SEND'         => 'Отправить',

    // Сообщение
    'MAIL_SUBJECT'      => 'Запрос с сайта ' . env('APP_URL'),
    'MAIL_MESSAGE_SENT' => 'Сообщение отправлено! Мы постараемся ответить как можно скорее.',
];