


/**
 * We'll load jQuery and the Bootstrap jQuery plugin which provides support
 * for JavaScript based Bootstrap features such as modals and tabs. This
 * code may be modified to fit the specific needs of your application.
 */

try {
    window.$ = window.jQuery = require('jquery');
    window.Popper = require('popper.js').default;
    require('bootstrap');
    require('../../../resources/assets/vendor/cookiejs/jquery.cookie.js');



    require('./hs.core.js');
    require('../../../resources/assets/js/components/hs.header.js');
    require('../../../resources/assets/vendor/fancybox/jquery.fancybox.min.js');
    require('../../../resources/assets/js/components/hs.popup.js');
    require('./hs.hamburgers.js');
    require('./hs.dropdown.js');
    require('./custom.js');



    // initialization of sidebar navigation component
    //$.HSCore.components.HSSideNav.init('.js-side-nav');

    // initialization of HSDropdown component
    //$.HSCore.components.HSDropdown.init($('[data-dropdown-target]'), {dropdownHideOnScroll: false});

    $(document).ready(function() {

        // initialization of HSScrollBar component
        //$.HSCore.components.HSScrollBar.init($('.js-scrollbar'));

        $("#accordion a").click(function() {
            var link = $(this);
            var closest_ul = link.closest("ul");
            var parallel_active_links = closest_ul.find(".active")
            var closest_li = link.closest("li");
            var link_status = closest_li.hasClass("active");
            var count = 0;

            closest_ul.find("ul").slideUp(function() {
                if (++count == closest_ul.find("ul").length)
                    parallel_active_links.removeClass("active");
            });

            if (!link_status) {
                closest_li.children("ul").slideDown();
                closest_li.addClass("active");
            }
        })
    })

    $(window).on('load', function () {

        // initialization of header
        $.HSCore.components.HSHeader.init($('#js-header'));
        $.HSCore.helpers.HSHamburgers.init('.hamburger');
        // initialization of popups
        $.HSCore.components.HSPopup.init('.js-fancybox');
    });


} catch (e) {alert(e);}

/**
 * We'll load the axios HTTP library which allows us to easily issue requests
 * to our Laravel back-end. This library automatically handles sending the
 * CSRF token as a header based on the value of the "XSRF" token cookie.
 */


/**
 * Echo exposes an expressive API for subscribing to channels and listening
 * for events that are broadcast by Laravel. Echo and event broadcasting
 * allows your team to easily build robust real-time web applications.
 */

// import Echo from 'laravel-echo'

// window.Pusher = require('pusher-js');

// window.Echo = new Echo({
//     broadcaster: 'pusher',
//     key: process.env.MIX_PUSHER_APP_KEY,
//     cluster: process.env.MIX_PUSHER_APP_CLUSTER,
//     encrypted: true
// });


