try {
    //window.Popper = require('popper.js').default;
    window.$ = window.jQuery = require('jquery');
    //require('bootstrap'); // from node_modules v4.*
    // admin-lte use bootstrap v 3.4.*
    require('admin-lte');
    require('admin-lte/bower_components/bootstrap/dist/js/bootstrap.min.js');
    require('admin-lte/plugins/iCheck/icheck.min');
    require('admin-lte/bower_components/datatables.net/js/jquery.dataTables.min.js');
    require('admin-lte/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js');
    require('admin-lte/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js');
    require('../../../resources/assets/vendor/fancybox/jquery.fancybox.min.js');

    // TinyMCE
    require('tinymce/tinymce.min.js');
    require('tinymce/themes/silver/theme.min.js');
    require('tinymce/plugins/link/plugin.min.js');
    require('tinymce/plugins/image/plugin.min.js');
    require('tinymce/plugins/imagetools/plugin.min.js');
    require('tinymce/plugins/code/plugin.min');



} catch (e) {}


/**
 * Echo exposes an expressive API for subscribing to channels and listening
 * for events that are broadcast by Laravel. Echo and event broadcasting
 * allows your team to easily build robust real-time web applications.
 */

// import Echo from 'laravel-echo'

// window.Pusher = require('pusher-js');

// window.Echo = new Echo({
//     broadcaster: 'pusher',
//     key: process.env.MIX_PUSHER_APP_KEY,
//     cluster: process.env.MIX_PUSHER_APP_CLUSTER,
//     encrypted: true
// });

$('input').iCheck({

    checkboxClass: 'icheckbox_square-blue',
    radioClass: 'iradio_square-blue',
    increaseArea: '20%' /* optional */
});


$(".fancybox").fancybox();

//$('#flash-overlay-modal').modal();
$('div.alert').not('.alert-important').delay(3000).fadeOut(350);

$('#table1').DataTable({
    "language": {
        "url": "//cdn.datatables.net/plug-ins/1.10.20/i18n/Russian.json"
    }
});
/*$('#table2').DataTable({
    'paging'      : true,
    'lengthChange': false,
    'searching'   : false,
    'ordering'    : true,
    'info'        : true,
    'autoWidth'   : false
});*/

function elFinderBrowser (field_name, url, type, win) {
    tinymce.activeEditor.windowManager.open({
        file: ' route(\'elfinder.tinymce4\') ',// use an absolute path! //TODO Добавить путь к файл-менеджеру
        title: 'elFinder 2.0',
        width: 900,
        height: 450,
        resizable: 'yes'
}, {
        setUrl: function (url) {
            win.document.getElementById(field_name).value = url;
        }
    });
    return false;
}

tinymce.init({
    selector: '.htmleditor',
    plugins: 'link image imagetools code',
    //menubar: false,
    toolbar: [
        'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | link image | code',

    ]
    //,
    //file_picker_callback : mceElf.browser,
    //images_upload_handler: mceElf.uploadHandler


});