const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.setPublicPath('public/build')
    .setResourceRoot('/build/')
    .js('resources/assets/js/app.js', 'js')
    .js('resources/assets/js/admin.js', 'js')
    .sass('resources/assets/sass/admin.scss', 'css')
    .sass('resources/assets/sass/app.scss', 'css')
    /*.styles([
        'resources/assets/vendor/bootstrap/bootstrap.min.css',
        'resources/assets/vendor/icon-awesome/css/font-awesome.min.css',
        'resources/assets/vendor/icon-line-pro/style.css',
        'resources/assets/vendor/icon-hs/style.css',
        'resources/assets/vendor/hamburgers/hamburgers.min.css',
        'resources/assets/css/unify-admin.css',
        'resources/assets/css/hs-admin-icons.css',
        //'resources/assets/css/unify-core.css',
        //'resources/assets/css/unify-components.css',
        //'resources/assets/css/unify-globals.css',
        'resources/assets/css/styles.op-corporate.css',
        'resources/assets/css/custom.css',
    ], 'public/build/css/styles.css')*/
    .copyDirectory('node_modules/tinymce/skins', 'js/skins')
    .version();


