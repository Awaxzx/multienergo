<?php

Route::get('/', function () {
    return view('welcome');
});

Route::get('admin/login', 'Admin\Auth\LoginController@showLoginForm')->name('admin.login');
Route::post('admin/login', 'Admin\Auth\LoginController@login');

Route::group(
    [
        'prefix'     => 'admin',
        'as'         => 'admin.',
        'namespace'  => 'Admin',
        'middleware' => [
            'auth',
            'can:admin-panel'
        ],
    ],
    function () {

        Route::get('/', 'DashboardController@index')->name('dashboard');

        Route::post('logout', 'Auth\LoginController@logout')->name('logout');

        // Route::resource('user', 'UserController')->name('index', 'user');
        Route::resource('user', 'Users\UserController')->name('index', 'user');
        Route::resource('page', 'Pages\PageController')->name('index', 'page');
        Route::resource('category', 'Catalog\CategoryController')->name('index', 'category');
        Route::resource('product', 'Catalog\ProductController')->name('index', 'product');
        Route::resource('certificate', 'Certificates\CertificateController')->name('index', 'certificate');


        Route::resource('files', 'Common\FileController')->name('index', 'files');

        //Route::get('files/create', 'Common\FileController@create')->name('files.create');
        //Route::post('files/create', 'Common\FileController@store')->name('files.store');
        //Route::get('files/{id}/edit', 'Common\FileController@edit')->name('files.edit');
        //Route::post('files/{id}/edit', 'Common\FileController@update')->name('files.update');


        //Route::post('files/create', 'Common\FileController@store')->name('store', 'files');

        //Route::get('files', 'Common\FileController@index')->name('files.list');
        //Route::post('files', 'Common\FileController@upload')->name('files.upload');

    }
);
//Auth::routes();

Route::get('/', 'PagesController@index')->name('home');

Route::get('/catalog.html', 'CatalogController@index')->name('catalog.index');

Route::group([
    'prefix' => 'catalog',
    'as'     => 'catalog.',
],
    function () {

        Route::get('/product/{uri}.html', 'CatalogController@product')->name('product');
        Route::get('/{fulluri}.html', 'CatalogController@category')->where('fulluri', '[A-Za-z\_\-\/0-9\.]+')->name('category');

    });

Route::get('/contacts.html', array('as' => 'contacts.index', 'uses' => 'ContactsController@index'));
Route::post('/contacts.html', array('as' => 'contacts.send', 'uses' => 'ContactsController@send'));
Route::get('/certificates.html', array('as' => 'certificates.index', 'uses' => 'CertificatesController@index'));
Route::get('/normative-acts.html', array('as' => 'acts.index', 'uses' => 'ActsController@index'));
Route::get('/{fulluri}.html', array('as' => 'pages.page', 'uses' => 'PagesController@page'))->where('fulluri', '[A-Za-z\_\-\/0-9]+');
//Route::get('/', 'PagesController@index')->name('home');
