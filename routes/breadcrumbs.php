<?php

use DaveJamesMiller\Breadcrumbs\Facades\Breadcrumbs;
use \App\Models\User;
use \App\Models\Page;
use \App\Models\Category;
use \App\Models\Product;
use \App\Models\Certificate;

// Admin
// Home
Breadcrumbs::for('admin.dashboard', function ($trail) {
    $trail->push(__('admin.MENU_DASHBOARD'), route('admin.dashboard'));
});

// Home > Users
Breadcrumbs::for('admin.user', function ($trail) {
    $trail->parent('admin.dashboard');
    $trail->push(__('admin.users.TTL_USERS'), route('admin.user'));
});

Breadcrumbs::for('admin.user.show', function ($trail, User $user) {
    $trail->parent('admin.user');
    $trail->push(__('admin.users.TTL_VIEW_USER'), route('admin.user.show', $user));
});


Breadcrumbs::for('admin.user.create', function ($trail) {
    $trail->parent('admin.user');
    $trail->push(__('admin.users.TTL_NEW_USER'), route('admin.user.create'));
});


Breadcrumbs::for('admin.user.edit', function ($trail, User $user) {
    $trail->parent('admin.user');
    $trail->push(__('admin.users.TTL_EDIT_USER'), route('admin.page.edit', ['user' => $user]));
});


// Home > Pages
Breadcrumbs::for('admin.page', function ($trail) {
    $trail->parent('admin.dashboard');
    $trail->push(__('admin.pages.TTL_PAGES'), route('admin.page'));
});

Breadcrumbs::for('admin.page.show', function ($trail, Page $page) {
    $trail->parent('admin.page');
    $trail->push(__('admin.pages.TTL_SHOW_PAGE'), route('admin.page.show', $page));
});


Breadcrumbs::for('admin.page.create', function ($trail) {
    $trail->parent('admin.page');
    $trail->push(__('admin.pages.TTL_NEW_PAGE'), route('admin.page.create'));
});


Breadcrumbs::for('admin.page.edit', function ($trail, Page $page) {
    $trail->parent('admin.page');
    $trail->push(__('admin.pages.TTL_EDIT_PAGE'), route('admin.page.edit', ['page' => $page]));
});




// Home > Categories
Breadcrumbs::for('admin.category', function ($trail) {
    $trail->parent('admin.dashboard');
    $trail->push(__('admin.categs.TTL_CATEGORIES'), route('admin.category'));
});

Breadcrumbs::for('admin.category.show', function ($trail, Category $category) {
    $trail->parent('admin.category');
    $trail->push(__('admin.categs.TTL_VIEW_CATEGORY'), route('admin.category.show', $category));
});


Breadcrumbs::for('admin.category.create', function ($trail) {
    $trail->parent('admin.category');
    $trail->push(__('admin.categs.TTL_NEW_CATEGORY'), route('admin.category.create'));
});


Breadcrumbs::for('admin.category.edit', function ($trail, Category $category) {
    $trail->parent('admin.category');
    $trail->push(__('admin.categs.TTL_EDIT_CATEGORY'), route('admin.category.edit', ['category' => $category]));
});

// Home > Products
Breadcrumbs::for('admin.product', function ($trail) {
    $trail->parent('admin.dashboard');
    $trail->push(__('admin.products.TTL_PRODUCTS'), route('admin.product'));
});

Breadcrumbs::for('admin.product.show', function ($trail, Product $product) {
    $trail->parent('admin.product');
    $trail->push(__('admin.products.TTL_VIEW_PRODUCT'), route('admin.product.show', $product));
});


Breadcrumbs::for('admin.product.create', function ($trail) {
    $trail->parent('admin.product');
    $trail->push(__('admin.products.TTL_NEW_PRODUCT'), route('admin.product.create'));
});


Breadcrumbs::for('admin.product.edit', function ($trail, Product $product) {
    $trail->parent('admin.product');
    $trail->push(__('admin.products.TTL_EDIT_PRODUCT'), route('admin.product.edit', ['product' => $product]));
});

// Home > Certificates
Breadcrumbs::for('admin.certificate', function ($trail) {
    $trail->parent('admin.dashboard');
    $trail->push(__('admin.certificates.TTL_CERTIFICATES'), route('admin.certificate'));
});

Breadcrumbs::for('admin.certificate.show', function ($trail, Certificate $certificate) {
    $trail->parent('admin.certificate');
    $trail->push(__('admin.certificates.TTL_VIEW_CERTIFICATE'), route('admin.certificate.show', $certificate));
});


Breadcrumbs::for('admin.certificate.create', function ($trail) {
    $trail->parent('admin.certificate');
    $trail->push(__('admin.certificates.TTL_NEW_CERTIFICATE'), route('admin.certificate.create'));
});


Breadcrumbs::for('admin.certificate.edit', function ($trail, Certificate $certificate) {
    $trail->parent('admin.certificate');
    $trail->push(__('admin.certificates.TTL_EDIT_CERTIFICATE'), route('admin.certificate.edit', ['certificate' => $certificate]));
});