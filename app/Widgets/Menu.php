<?php

namespace App\Widgets;

use App\Models\Page;
use Arrilot\Widgets\AbstractWidget;

class Menu extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config
        = [
            'type'     => 'top',
            'template' => 'top',
        ];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {

        $pages = Page::active()->with(['menus' => function ($q) {
            $q->where('code', $this->config['type']);
        }])->orderBy('sort')->get();

        return view('widgets.menu_' . $this->config['template'], [
            'config' => $this->config,
            'pages'  => $pages
        ]);
    }
}
