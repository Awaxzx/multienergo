<?php

namespace App\Widgets;

use App\Models\CatalogTree;
use App\Models\Category;
use Arrilot\Widgets\AbstractWidget;
use Illuminate\Http\Request;

class CatalogMenu extends AbstractWidget
{
    /**
     * The configuration array.
     *
     * @var array
     */
    protected $config
        = [
            'currentCategoryId' => 0,
            'depth'             => 0,
            'template'          => 'default',
        ];

    /**
     * Treat this method as a controller action.
     * Return view() or other content to display.
     */
    public function run()
    {
        $catalogTree = new CatalogTree();
        if ($this->config['currentCategoryId'])
        {
            $catalogTree->setCurrentCategoryId($this->config['currentCategoryId']);
        }

        if ($this->config['depth'] > 0)
        {
            $catalogTree->setDepth($this->config['depth']);
        }

        $tree = $catalogTree->getTree();

        //dd($tree);

        return view('widgets.catalog_menu_' . $this->config['template'], [
            'config' => $this->config,
            'tree'   => $tree,
        ]);
    }
}
