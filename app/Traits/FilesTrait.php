<?php

namespace App\Traits;

use App\Models\File;

Trait FilesTrait
{
    public function files()
    {
        return $this->hasMany(File::class, 'owner_id')->where('owner_class', self::class);
    }

    public function getFiles()
    {
        $files = $this->files;
        if (!$files) $files = [new File()];

        return $files;
    }


    public function saveFile(array $fileAttributes)
    {
        $meta = $this->getFiles();
        $meta->setRawAttributes($fileAttributes);
        $meta->owner_class = self::class;
        $meta->owner_id    = $this->id;
        $meta->save();
    }

    public function deleteFile()
    {
        $files = $this->getFiles();
        $files->delete();
    }
}