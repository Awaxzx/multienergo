<?php

namespace App\Traits;

Trait ImageAndThumbTrait
{
    public function getImage()
    {
        if($this->image)
        {
            return self::IMAGE_DIRECTORY. '/' . $this->image;
        }

        return false;
    }

    public function getThumbnail()
    {
        if($this->image)
        {
            return self::THUMB_DIRECTORY . '/' . $this->image;
        }

        return false;
    }
}