<?php
/**
 * Created by PhpStorm.
 * User: awax
 * Date: 02.10.19
 * Time: 11:58
 */

namespace App\Traits;

trait FlatTreeTrait
{
    protected static $tree = [];

    public static function getFlatTree($parent_id = 0, $level = 0, $excludeId = 0, $activeOnly = false)
    {
        $query = self::where(['parent_id' => $parent_id])
                     ->where('id', '<>', $excludeId)
                     ->orderBy('sort');

        if ($activeOnly)
        {
            $query->where('status', self::STATUS_ACTIVE);
        }

        $items = $query->get();

        if ($items)
        {
            if ($parent_id == 0)
            {
                $level = 0;
            }
            else
            {
                $level++;
            }

            foreach ($items as $item)
            {
                if ($item->parent_id == 0) $level = 0;

                self::$tree[$item->id]['id']      = $item->id;
                self::$tree[$item->id]['name']    = $item->name;
                self::$tree[$item->id]['fulluri'] = $item->fulluri;
                self::$tree[$item->id]['content'] = $item->content;
                self::$tree[$item->id]['offset']  = str_repeat('-', $level * 2);
                self::getFlatTree($item->id, $level, $excludeId);
            }

        }

        return self::$tree;
    }


    /** Рекурсивно обновляет full_uri потомков
     * @param integer $id - идентификатор
     * @param string $item_uri - uri
     */
    public static function fixChildrenUrls($id, $item_uri)
    {
        $children = self::where(['parent_id' => $id])->get();

        foreach ($children as $child)
        {
            $full_uri = $item_uri . "/" . $child->uri;
            $child->update(['fulluri' => $full_uri]);

            self::fixChildrenUrls($child->id, $full_uri);
            unset($child);
        }
    }

    /** Формирует значенеи full_uri на основе id родителя и данного uri
     * @param integer $parent_id - id родителя
     * @param string $uri - собственно uri
     * @return string  - значение параметра full_uri
     */
    public static function makeFullUri($parent_id, $uri)
    {
        $parent = self::where(['id' => $parent_id])->first();

        if (!$parent OR $parent->fulluri == '/')
        {
            return $uri;
        }

        return $parent->fulluri . '/' . $uri;
    }
}