<?php

namespace App\Traits;

use App\Models\Meta;

Trait MetaTagsTrait
{
    public function meta()
    {
        return $this->hasOne(Meta::class, 'owner_id')->where('owner_class', self::class);
    }

    public function getMeta()
    {
        $meta = $this->meta;
        if (!$meta) $meta = new Meta();

        return $meta;
    }

    public function getMetaTitle()
    {
        $meta = $this->getMeta();
        return $meta->title;
    }

    public function getMetaKeywords()
    {
        $meta = $this->getMeta();
        return $meta->keywords;
    }

    public function getMetaDescription()
    {
        $meta = $this->getMeta();
        return $meta->description;
    }

    public function saveMeta(array $metaAttributes)
    {
        $meta = $this->getMeta();
        $meta->setRawAttributes($metaAttributes);
        $meta->owner_class = self::class;
        $meta->owner_id    = $this->id;
        $meta->save();
    }

    public function deleteMeta()
    {
        $meta = $this->getMeta();
        $meta->delete();
    }
}