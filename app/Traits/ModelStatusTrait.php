<?php
/**
 * Created by PhpStorm.
 * User: awax
 * Date: 29.08.19
 * Time: 16:47
 */

namespace App\Traits;

trait ModelStatusTrait
{
    public static function statuses()
    {
        $reflectionClass = new \ReflectionClass(static::class);
        $constants = $reflectionClass->getConstants();
        $statuses = [];
        foreach($constants as $name => $value)
        {
            if(mb_substr($name, 0, 7) == 'STATUS_')
            {
                $statuses[$name] = $value;
            }
        }

        return $statuses;

    }

    public static function active()
    {
        return self::where('status', self::STATUS_ACTIVE);
    }

}