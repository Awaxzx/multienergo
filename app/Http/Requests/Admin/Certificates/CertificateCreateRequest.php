<?php

namespace App\Http\Requests\Admin\Certificates;

use Illuminate\Foundation\Http\FormRequest;

class CertificateCreateRequest extends FormRequest
{
    public function authorize()
    : bool
    {
        return true;
    }

    public function rules()
    : array
    {
        return [
            'name'   => 'required|string|max:255',
            'image'  => 'required|mimes:jpg,jpeg,png',
            'sort'   => 'integer',
            'status' => 'string|max:25',
        ];
    }
}