<?php

namespace App\Http\Requests\Admin\Certificates;

use Illuminate\Foundation\Http\FormRequest;

class CertificateUpdateRequest extends FormRequest
{
    public function authorize()
    : bool
    {
        return true;
    }

    public function rules()
    : array
    {
        return [
            'name'   => 'required|string|max:255',
            'image'  => 'mimes:jpg,jpeg,png',
            'sort'   => 'integer',
            'status' => 'string|max:25',
        ];
    }
}