<?php

namespace App\Http\Requests\Admin\Files;

use Illuminate\Foundation\Http\FormRequest;

class UpdateFileRequest extends FormRequest
{
    public function authorize()
    : bool
    {
        return true;
    }

    public function rules()
    : array
    {
        return [
            'file.title'       => 'required|string|max:255',
            'file.description' => 'string|nullable|max:65535',
            'file.name'        => 'mimes:zip,rar,pdf,doc,docx,xls,xlsx',
            'file.sort'        => 'integer',
            'file.status'      => 'string|max:25',
        ];
    }
}