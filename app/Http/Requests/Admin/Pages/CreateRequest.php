<?php

namespace App\Http\Requests\Admin\Pages;

use Illuminate\Foundation\Http\FormRequest;

class CreateRequest extends FormRequest
{
    public function authorize()
    : bool
    {
        return true;
    }

    public function rules()
    : array
    {
        return [
            'parent_id'        => 'integer',
            'name'             => 'required|string|max:255',
            'uri'              => 'required|string|max:255|unique:pages',
            'fulluri'          => 'string|max:255',
            'content'          => 'string|nullable|max:4294967295',
            'sort'             => 'integer',
            'status'           => 'string|max:25',
            'meta.title'       => 'required|string|max:255',
            'meta.keywords'    => 'string|nullable|max:255',
            'meta.description' => 'string|nullable|max:255',
        ];
    }
}