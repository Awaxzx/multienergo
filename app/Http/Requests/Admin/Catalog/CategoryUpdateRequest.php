<?php

namespace App\Http\Requests\Admin\Catalog;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class CategoryUpdateRequest extends FormRequest
{
    public function authorize()
    : bool
    {
        return true;
    }

    public function rules()
    : array
    {
        return [
            'parent_id' => 'integer',
            'name'      => 'required|string|max:255',
            'uri'       => ['required', 'string', 'max:255',
                            Rule::unique('categories')->ignore($this->category->id)
            ],
            'fulluri'   => 'string|max:255',
            'content'   => 'string|nullable|max:4294967295',
            'sort'      => 'integer',
            'status'    => 'string|max:25',
            'meta.title'       => 'required|string|max:255',
            'meta.keywords'    => 'string|nullable|max:255',
            'meta.description' => 'string|nullable|max:255',
        ];
    }
}