<?php

namespace App\Http\Requests\Admin\Catalog;

use Illuminate\Foundation\Http\FormRequest;

class ProductCreateRequest extends FormRequest
{
    public function authorize()
    : bool
    {
        return true;
    }

    public function rules()
    : array
    {
        return [
            'category_id'   => 'required|integer|not_in:0',
            'name'          => 'required|string|max:255',
            'uri'           => 'required|string|max:255|unique:products',
            'short_content' => 'string|max:65535',
            'content'       => 'string|max:4294967295',
            'image'         => 'mimes:jpg,jpeg,png',
            'sort'          => 'integer',
            'status'        => 'string|max:25',
            'meta.title'       => 'required|string|max:255',
            'meta.keywords'    => 'string|nullable|max:255',
            'meta.description' => 'string|nullable|max:255',
        ];
    }
}