<?php

namespace App\Http\Controllers;

use App\Models\Page;
use Illuminate\Http\Request;

class ActsController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        $uri   = substr($request->segment(1), 0, -5);
        $page  = Page::active()->where('uri', $uri)->first();
        $files = $page->getFiles();
        $this->setMetaTags($page->getMeta());

        return view('pages.acts', ['page' => $page, 'files' => $files]);
    }


}