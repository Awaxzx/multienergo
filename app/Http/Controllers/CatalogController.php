<?php

namespace App\Http\Controllers;

use App\Models\CatalogTree;
use App\Models\Category;
use App\Models\Page;
use App\Models\Product;

class CatalogController extends Controller
{
    /**
     *
     *
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $page       = Page::active()->where('uri', 'catalog')->first();
        $this->setMetaTags($page->getMeta());


        $tree = (new CatalogTree())->withProducts(3)
                                   ->withFiles()
                                   ->setDepth(2)
                                   ->getTree();

        //dd($tree);

        return view('catalog.home', ['page' => $page, 'categories' => $tree, 'currentCategoryId' => array_first($tree)]);
    }

    public function category($fulluri)
    {
        $category   = Category::active()->with('products')->where('fulluri', $fulluri)->first();
        $this->setMetaTags($category->getMeta());

        $tree = (new CatalogTree())->setCategoryId($category->id)
                                    ->withProducts()->withFiles()->getTree();

        return view('catalog.category', ['categories' => $tree, 'currentCategoryId' => $category->id]);
    }

    public function product($product_uri)
    {
        $product    = Product::active()->with('category')->where('uri', $product_uri)->first();
        $this->setMetaTags($product->getMeta());
        return view('catalog.product', ['product' => $product]);
    }
}