<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DashboardController extends Controller
{
    //

    public function index()
    {
        //flash('Welcome!')->info()->important();


        return view('admin.dashboard.dashboard');
    }
}
