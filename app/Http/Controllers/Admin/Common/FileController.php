<?php

namespace App\Http\Controllers\Admin\Common;

use App\Http\Requests\Admin\Files\CreateFileRequest;
use App\Http\Requests\Admin\Files\UpdateFileRequest;
use App\Models\File;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;


class FileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $files = File::where(['owner_class' => $request->owner_class, 'owner_id' => $request->owner_id])->orderBy('sort')->get();
        return view('admin.files._list', [
            'files'       => $files,
            'owner_id'    => $request->owner_id,
            'owner_class' => $request->owner_class
        ])->render();
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return response()->json([
            'form'   => view('admin.files._form', ['file' => new File()])->render(),
            'action' => route('admin.files.store'),
            'method' => 'post',
            'title'  => __('admin.files.TTL_ADD_FILE'),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\Admin\Files\CreateFileRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateFileRequest $request)
    {
        $fileName = '';
        $fileType = '';
        if ($request->file('file.name'))
        {
            $fileName = $this->_saveFile($request);
            $fileType = $request->file('file.name')->getClientOriginalExtension();
        }

        $file              = new File();
        $file->name        = $fileName;
        $file->type        = $fileType;
        $file->owner_id    = $request->file['owner_id'];
        $file->owner_class = $request->file['owner_class'];
        $file->title       = $request->file['title'];
        $file->description = $request->file['description'];
        $file->sort        = $request->file['sort'];
        $file->status      = $request->file['status'];
        $file->save();

        return response()->json(['success' => __('admin.files.MSG_FILE_UPLOADED')]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  File $file
     * @return \Illuminate\Http\Response
     */
    public function edit(File $file)
    {
        return response()->json([
            'form'   => view('admin.files._form', ['file' => $file])->render(),
            'action' => route('admin.files.update', ['file' => $file]),
            'method' => 'put',
            'title'  => __('admin.files.TTL_EDIT_FILE'),
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  App\Http\Requests\Admin\Files\UpdateFileRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateFileRequest $request, File $file)
    {
        if ($request->file('file.name'))
        {
            $fileName = $this->_saveFile($request);
            $fileType = $request->file('file.name')->getClientOriginalExtension();
        }
        else
        {
            $fileName = $file->name;
            $fileType = $file->type;
        }

        $file->name        = $fileName;
        $file->type        = $fileType;
        $file->title       = $request->file['title'];
        $file->description = $request->file['description'];
        $file->sort        = $request->file['sort'];
        $file->status      = $request->file['status'];
        $file->save();

        return response()->json(['success' => __('admin.files.MSG_FILE_UPDATED')]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(File $file)
    {
        Storage::disk('public')->delete($file->getFile());
        $file->delete();

        return response()->json(['success' => __('admin.files.MSG_FILE_DELETED')]);
    }

    private function _saveFile($request)
    {
        $fileName = $request->file('file.name')->getClientOriginalName();
        $request->file('file.name')->storeAs(File::FILES_DIRECTORY, $fileName, 'public');

        return $fileName;
    }
}
