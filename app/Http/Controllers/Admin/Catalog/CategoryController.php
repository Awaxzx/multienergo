<?php

namespace App\Http\Controllers\Admin\Catalog;

use App\Http\Requests\Admin\Catalog\CategoryCreateRequest;
use App\Http\Requests\Admin\Catalog\CategoryUpdateRequest;
use App\Models\Category;
use App\Http\Controllers\Controller;
use Illuminate\Validation\Rule;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Category::all();
        return view('admin.catalog.category.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tree = Category::getFlatTree();
        return view('admin.catalog.category.create', ['category' => new Category(), 'tree' => $tree]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Requests\Admin\Catalog\CategoryCreateRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryCreateRequest $request)
    {

        $category = Category::make([
            'parent_id' => $request->parent_id,
            'name'      => $request->name,
            'uri'       => $request->uri,
            'fulluri'   => Category::makeFullUri($request->parent_id, $request->uri),
            'content'   => $request->get('content'),
            'image'     => $this->_saveImage($request),
            'sort'      => $request->sort,
            'status'    => $request->status,
        ]);

        $category->save();

        // Сохранение мета-тегов
        $category->saveMeta($request->meta);

        flash(__('admin.categs.MSG_CATEGORY_CREATED'))->success();

        return admin_redirect('admin.category', $category);
    }

    /**
     * Display the specified resource.
     *
     * @param  Category $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        return view('admin.catalog.category.show', ['category' => $category]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Category $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        $tree = Category::getFlatTree(0, 0, $category->id);
        return view('admin.catalog.category.edit', [
            'category' => $category,
            'tree'     => $tree]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Requests\Admin\Catalog\CategoryUpdateRequest $request
     * @param  Category $category
     * @return \Illuminate\Http\Response
     */
    public function update(CategoryUpdateRequest $request, Category $category)
    {
        $category->update([
            'parent_id' => $request->parent_id,
            'name'      => $request->name,
            'uri'       => $request->uri,
            'fulluri'   => Category::makeFullUri($request->parent_id, $request->uri),
            'content'   => $request->get('content'),
            'image'     => $this->_saveImage($request, $category->image),
            'sort'      => $request->sort,
            'status'    => $request->status,
        ]);

        Category::fixChildrenUrls($category->id, $category->fulluri);

        // Сохранение мета-тегов
        $category->saveMeta($request->meta);

        flash(__('admin.categs.MSG_CATEGORY_UPDATED'))->success();

        return admin_redirect('admin.category', $category);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Category $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        // Удаление мета
        $category->deleteMeta();

        $category->delete();
        flash(__('admin.categs.MSG_CATEGORY_DELETED'))->success();
        return redirect()->route('admin.category');
    }

    private function _saveImage($request, $defaultFileName = '')
    {
        if ($request->file('image'))
        {
            $fileName = $request->file('image')->getClientOriginalName();
            $request->file('image')->storeAs(Category::IMAGE_DIRECTORY, $fileName, 'public');
        }
        else
        {
            $fileName = $defaultFileName;
        }


        return $fileName;
    }
}
