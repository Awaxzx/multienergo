<?php

namespace App\Http\Controllers\Admin\Catalog;

use App\Http\Requests\Admin\Catalog\ProductCreateRequest;
use App\Http\Requests\Admin\Catalog\ProductUpdateRequest;
use App\Models\Category;
use App\Models\Product;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Image;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::all();
        return view('admin.catalog.product.index', compact('products'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tree = Category::getFlatTree();
        return view('admin.catalog.product.create', ['product' => new Product(), 'tree' => $tree]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Requests\Admin\Catalog\ProductCreateRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(ProductCreateRequest $request)
    {
        $product = Product::make([
            'category_id'   => $request->category_id,
            'name'          => $request->name,
            'uri'           => $request->uri,
            'short_content' => $request->short_content,
            'content'       => $request->get('content'),
            'image'         => $this->_saveImage($request),
            'sort'          => $request->sort,
            'status'        => $request->status,
        ]);

        $product->save();

        // Сохранение мета-тегов
        $product->saveMeta($request->meta);

        flash(__('admin.products.MSG_PRODUCT_CREATED'))->success();

        return admin_redirect('admin.product', $product);
    }

    /**
     * Display the specified resource.
     *
     * @param  Product $product
     * @return \Illuminate\Http\Response
     */
    public function show(Product $product)
    {
        return view('admin.catalog.product.show', ['product' => $product]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Product $product
     * @return \Illuminate\Http\Response
     */
    public function edit(Product $product)
    {
        $tree = Category::getFlatTree(0, 0);
        return view('admin.catalog.product.edit', ['product' => $product, 'tree' => $tree]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Requests\Admin\Catalog\ProductCreateRequest $request
     * @param  Product $product
     * @return \Illuminate\Http\Response
     */
    public function update(ProductUpdateRequest $request, Product $product)
    {
        $product->update([
            'category_id'   => $request->category_id,
            'name'          => $request->name,
            'uri'           => $request->uri,
            'short_content' => $request->short_content,
            'content'       => $request->get('content'),
            'image'         => $this->_saveImage($request, $product->image),
            'sort'          => $request->sort,
            'status'        => $request->status,
        ]);

        // Сохранение мета-тегов
        $product->saveMeta($request->meta);

        flash(__('admin.products.MSG_PRODUCT_UPDATED'))->success();

        return admin_redirect('admin.product', $product);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Product $product
     * @return \Illuminate\Http\Response
     */
    public function destroy(Product $product)
    {
        // Удаление мета
        $product->deleteMeta();

        // Удаляем файлы
        Storage::disk('public')->delete($product->getImage());
        Storage::disk('public')->delete($product->getThumbnail());

        $product->delete();
        flash(__('admin.products.MSG_PRODUCT_DELETED'))->success();
        return redirect()->route('admin.product');
    }


    /** Сохраняет изображение, генерирует превьюшку и возвращает имя файла если файл загружен или дефолтное имя файла, если задано
     * @param $request
     * @param string $defaultFileName
     * @return string
     */
    private function _saveImage($request, $defaultFileName = '')
    {
        if ($request->file('image'))
        {

            $fileName  = $request->file('image')->getClientOriginalName();
            $imagePath = $request->file('image')->storeAs(Product::IMAGE_DIRECTORY, $fileName, 'public');

            // Make thumbnail
            Image::make(Storage::disk('public')->path($imagePath))->resize(Product::THUMB_WIDTH, Product::THUMB_HEIGHT,
                function ($constraint) {
                    $constraint->aspectRatio();
                }
            )->save(Storage::disk('public')->path(Product::THUMB_DIRECTORY . '/' . $fileName));

        }
        else
        {
            $fileName = $defaultFileName;
        }

        return $fileName;
    }
}
