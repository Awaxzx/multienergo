<?php

namespace App\Http\Controllers\Admin\Certificates;

use App\Http\Requests\Admin\Certificates\CertificateCreateRequest;
use App\Http\Requests\Admin\Certificates\CertificateUpdateRequest;
use App\Models\Certificate;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Image;

class CertificateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $certificates = Certificate::all();
        return view('admin.certificate.index', compact('certificates'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.certificate.create', ['certificate' => new Certificate()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(CertificateCreateRequest $request)
    {
        $certificate = Certificate::make([
            'name'   => $request->name,
            'image'  => $this->_saveImage($request),
            'sort'   => $request->sort,
            'status' => $request->status,
        ]);

        $certificate->save();


        flash(__('admin.certificates.MSG_CERTIFICATE_CREATED'))->success();

        return admin_redirect('admin.certificate', $certificate);
    }

    /**
     * Display the specified resource.
     *
     * @param  App\Models\Certificate
     * @return \Illuminate\Http\Response
     */
    public function show(Certificate $certificate)
    {
        return view('admin.certificate.show', ['certificate' => $certificate]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  App\Models\Certificate
     * @return \Illuminate\Http\Response
     */
    public function edit(Certificate $certificate)
    {
        return view('admin.certificate.edit', ['certificate' => $certificate]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(CertificateUpdateRequest $request, Certificate $certificate)
    {
        $certificate->update([
            'name'   => $request->name,
            'image'  => $this->_saveImage($request, $certificate->image),
            'sort'   => $request->sort,
            'status' => $request->status,
        ]);

        flash(__('admin.certificates.MSG_CERTIFICATE_UPDATED'))->success();

        return admin_redirect('admin.certificate', $certificate);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Certificate $certificate
     * @return \Illuminate\Http\Response
     */
    public function destroy(Certificate $certificate)
    {

        // Удаляем файлы
        Storage::disk('public')->delete($certificate->getImage());
        Storage::disk('public')->delete($certificate->getThumbnail());

        $certificate->delete();

        flash(__('admin.certificates.MSG_CERTIFICATE_DELETED'))->success();

        return redirect()->route('admin.certificate');
    }


    private function _saveImage($request, $defaultFileName = '')
    {
        if ($request->file('image'))
        {
            $fileName  = $request->file('image')->getClientOriginalName();
            $imagePath = $request->file('image')->storeAs(Certificate::IMAGE_DIRECTORY, $fileName, 'public');

            // Make thumbnail
            Image::make(Storage::disk('public')->path($imagePath))->resize(Certificate::THUMB_WIDTH, Certificate::THUMB_HEIGHT,
                function ($constraint) {
                    $constraint->aspectRatio();
                }
            )->save(Storage::disk('public')->path(Certificate::THUMB_DIRECTORY . '/' . $fileName));

        }
        else
        {
            $fileName = $defaultFileName;
        }
        return $fileName;
    }

}
