<?php

namespace App\Http\Controllers\Admin\Pages;

use App\Http\Requests\Admin\Pages\CreateRequest;
use App\Http\Requests\Admin\Pages\UpdateRequest;
use App\Models\Page;
use App\Models\Menu;
use App\Http\Controllers\Controller;

class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pages = Page::all();
        return view('admin.pages.page.index', compact('pages'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $tree  = Page::getFlatTree();
        $menus = Menu::active()->orderBy('sort', 'asc')->get();
        return view('admin.pages.page.create', [
            'page'  => new Page(),
            'tree'  => $tree,
            'menus' => $menus
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateRequest $request)
    {
        $page = Page::make([
            'parent_id' => $request->parent_id,
            'name'      => $request->name,
            'uri'       => $request->uri,
            'fulluri'   => Page::makeFullUri($request->parent_id, $request->uri),
            'content'   => $request->get('content'),
            'sort'      => $request->sort,
            'status'    => $request->status,
        ]);

        $page->save();

        // сохранение меню
        $page->menus()->attach($request->menu);

        // Сохранение мета-тегов
        $page->saveMeta($request->meta);

        flash(__('admin.pages.MSG_PAGE_CREATED'))->success();

        return admin_redirect('admin.page', $page);
    }

    /**
     * Display the specified resource.
     *
     * @param  Page $page
     * @return \Illuminate\Http\Response
     */
    public function show(Page $page)
    {
        return view('admin.pages.page.show', ['page' => $page]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  Page $page
     * @return \Illuminate\Http\Response
     */
    public function edit(Page $page)
    {
        $tree  = Page::getFlatTree(0, 0, $page->id);
        $menus = Menu::active()->orderBy('sort', 'asc')->get();

        return view('admin.pages.page.edit', [
            'page'  => $page,
            'tree'  => $tree,
            'menus' => $menus,
            ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  App\Http\Requests\Admin\Pages\UpdateRequest $request
     * @param  \App\Models\Page $page
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateRequest $request, Page $page)
    {
        $page->update([
            'parent_id' => $request->parent_id,
            'name'      => $request->name,
            'uri'       => $request->uri,
            'fulluri'   => Page::makeFullUri($request->parent_id, $request->uri),
            'content'   => $request->get('content'),
            'sort'      => $request->sort,
            'status'    => $request->status,
        ]);

        Page::fixChildrenUrls($page->id, $page->fulluri);

        // сохранение меню
        $page->menus()->detach();
        $page->menus()->attach($request->menu);

        // Сохранение мета-тегов
        $page->saveMeta($request->meta);


        flash(__('admin.pages.MSG_PAGE_UPDATED'))->success();

        return admin_redirect('admin.page', $page);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Page $page
     * @return \Illuminate\Http\Response
     */
    public function destroy(Page $page)
    {
        // Удаление мета
        $page->deleteMeta();

        // Удаление страницы
        $page->delete();

        flash(__('admin.pages.MSG_PAGE_DELETED'))->success();

        return redirect()->route('admin.page');
    }
}
