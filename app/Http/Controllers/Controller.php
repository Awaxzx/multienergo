<?php

namespace App\Http\Controllers;

use App\Models\Meta;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;


    public function setMetaTags(Meta $meta)
    {
        view()->share('meta_title', $meta->title);
        view()->share('meta_keywords', $meta->keywords);
        view()->share('meta_description', $meta->description);
    }
}
