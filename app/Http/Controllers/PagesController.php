<?php

namespace App\Http\Controllers;

use App\Models\Page;

class PagesController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $page = Page::active()->where(['fulluri' => '/'])->first();
        $this->setMetaTags($page->getMeta());

        return view('pages.home', ['page' => $page]);
    }


    public function page($fulluri)
    {
        $page = Page::active()->where(['fulluri' => $fulluri])->first();
        $this->setMetaTags($page->getMeta());
        return view('pages.page', ['page' => $page]);
    }
}