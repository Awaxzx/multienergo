<?php

namespace App\Http\Controllers;

use App\Models\Page;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class ContactsController extends Controller
{
    //
    public function index()
    {
        $page = Page::where('fulluri', 'contacts')->first();
        $this->setMetaTags($page->getMeta());
        return view('pages.contacts', ['page' => $page]);
    }

    public function send(Request $request)
    {
        $this->validate($request, [
            'name'    => 'required|string|max:255',
            'phone'   => 'string',
            'email'   => 'required|email|string|max:255',
            'subject' => 'string|max:255',
            'message' => 'required|max:4294967295',
        ]);

        Mail::send('emails.contact-message', [
            'name'    => $request->name,
            'email'   => $request->email,
            'phone'   => $request->phone,
            'subject' => $request->subject,
            'msg' => $request->message,
        ], function($mail) use ($request){

            $mail->from($request->email, $request->name);
            $mail->to(env('MAIL_TO'))->subject(__('site.MAIL_SUBJECT'));
        });

        return redirect()->back()->with('flash_message', __('site.MAIL_MESSAGE_SENT'));
    }
}
