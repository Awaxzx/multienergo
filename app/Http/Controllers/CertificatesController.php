<?php

namespace App\Http\Controllers;

use App\Models\Page;
use App\Models\Certificate;

class CertificatesController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $page         = Page::active()->where('uri', 'certificates')->first();
        $certificates = Certificate::active()->orderBy('sort')->get();
        $this->setMetaTags($page->getMeta());

        return view('pages.certificates', ['page' => $page, 'certificates' => $certificates]);
    }



}