<?php

use Illuminate\Database\Eloquent\Model;

if (!function_exists('admin_redirect'))
{

    function admin_redirect($routeName, Model $object)
    {

        if (isset(app('request')->CREATE))
        {
            return redirect()->route($routeName);
        }
        elseif (isset(app('request')->CREATE_AND_VIEW))
        {
            return redirect()->route($routeName . '.show', $object);
        }
        elseif (isset(app('request')->CREATE_AND_CREATE))
        {
            return redirect()->route($routeName . '.create');
        }
        elseif (isset(app('request')->CREATE_AND_UPDATE))
        {
            return redirect()->route($routeName . '.edit', $object);
        }
        elseif (isset(app('request')->UPDATE))
        {
            return redirect()->route($routeName);
        }
        elseif (isset(app('request')->UPDATE_AND_VIEW))
        {
            return redirect()->route($routeName . '.show', $object);
        }
        elseif (isset(app('request')->UPDATE_AND_CONTINUE))
        {
            return redirect()->route($routeName . '.edit', $object);
        }
        elseif (isset(app('request')->UPDATE_AND_CREATE))
        {
            return redirect()->route($routeName . '.create');
        }

    }
}