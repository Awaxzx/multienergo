<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Category extends Model
{
    use \App\Traits\ModelStatusTrait;
    use \App\Traits\FlatTreeTrait;
    use \App\Traits\MetaTagsTrait;
    use \App\Traits\FilesTrait;

    const STATUS_ACTIVE   = 'active';
    const STATUS_INACTIVE = 'inactive';

    const IMAGE_DIRECTORY = 'categories';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable
        = [
            'name', 'parent_id', 'uri', 'fulluri', 'image', 'content', 'sort', 'status',
        ];

    public static function active()
    {
        return self::where('status', self::STATUS_ACTIVE);
    }

    public function products()
    {
        return self::hasMany(Product::class);
    }

    public function getTree(Request $request, $parent_id = 0, $level = 0)
    {
        $path       = $this->_preparePath($request);
        $tree       = [];
        $openParent = false;
        $categories = self::where(['parent_id' => $parent_id, 'status' => self::STATUS_ACTIVE])->orderBy('sort')->get();
        if ($categories)
        {
            foreach ($categories as $category)
            {
                $level++;
                list($items, $openParent) = $this->getTree($request, $category->id, $level);

                foreach ($items as $item)
                {
                    if ($item['fulluri'] == $path OR $item['open'] == true)
                    {
                        $openParent = true;
                    }


                }

                $tree[$category->id] = [
                    'id'      => $category->id,
                    'name'    => $category->name,
                    'uri'     => $category->uri,
                    'fulluri' => $category->fulluri,
                    'content' => $category->content,
                    'items'   => $items,
                    'open'    => $openParent,
                    'submenu' => ($items) ? true : false,
                    'level'   => $level,
                ];
            }
        }


        return [$tree, $openParent];
    }


    private function _preparePath(Request $request)
    {
        $path = substr($request->decodedPath(), strlen($request->segment(1)) + 1, -5);

        return $path;
    }

    public function getImage()
    {
        if ($this->image)
        {
            return self::IMAGE_DIRECTORY . '/' . $this->image;
        }

        return false;
    }
}
