<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use \App\Traits\ModelStatusTrait;
    use \App\Traits\MetaTagsTrait;
    use \App\Traits\FilesTrait;
    use \App\Traits\ImageAndThumbTrait;

    const STATUS_ACTIVE   = 'active';
    const STATUS_INACTIVE = 'inactive';

    const IMAGE_DIRECTORY = 'products';
    const THUMB_DIRECTORY = self::IMAGE_DIRECTORY . '/thumbs';
    const THUMB_WIDTH     = 250;
    const THUMB_HEIGHT    = null;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable
        = [
            'name', 'category_id', 'uri', 'image', 'short_content', 'content', 'sort', 'status',
        ];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public static function active()
    {
        return self::where('status', self::STATUS_ACTIVE);
    }


}
