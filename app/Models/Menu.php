<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    use \App\Traits\ModelStatusTrait;

    const STATUS_ACTIVE   = 'active';
    const STATUS_INACTIVE = 'inactive';

    public function pages()
    {
        return $this->belongsToMany('App\Models\Pages', 'menu_pages');
    }

}
