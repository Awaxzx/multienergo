<?php
/**
 * Created by PhpStorm.
 * User: Awax
 * Date: 25.10.2019
 * Time: 14:06
 */

namespace App\Models;

use Illuminate\Http\Request;

Class CatalogTree
{
    private $category_id       = 0;
    private $categoryParent    = true;
    private $currentCategoryId = 0;
    private $depth             = 0;
    private $withProducts      = false;
    private $withFiles         = false;
    private $productsLimit     = 0;

    /**
     * @param $category_id -id категории
     * @return $this
     */
    public function setCategoryId($category_id = 0, $categoryParent = false)
    {
        $this->category_id    = $category_id;
        $this->categoryParent = $categoryParent;
        return $this;
    }

    /**
     * @param $category_id -id категории
     * @return $this
     */
    public function setCurrentCategoryId($category_id = 0)
    {
        $this->currentCategoryId = $category_id;
        return $this;
    }

    /**
     * @param Request $request -объект запроса для определения текущей категории
     * @return $this
     */
    /*public function setRequest(Request $request)
    {
        $this->request = $request;
        $this->path    = $this->_preparePath($request);
        return $this;
    }*/

    /**
     * @param int $depth - глубина выборки дерева, 0 - без ограничения
     * @return $this
     */
    public function setDepth($depth = 0)
    {
        $this->depth = $depth;
        return $this;
    }

    /** Выбирать ли продукты в категориях если они есть
     * @param int $limit - количество получаемых продуктов, 0 - без ограничения
     * @return $this
     */
    public function withProducts($limit = 0)
    {
        $this->withProducts  = true;
        $this->productsLimit = $limit;
        return $this;
    }

    /** Выбирать ли прикрепленные файлы в категориях если они есть
     * @return $this
     */
    public function withFiles()
    {
        $this->withFiles = true;
        return $this;
    }

    /** Получает из объекта запроса fulluri категории
     * @param Request $request
     * @return bool|string
     */
    /*private function _preparePath(Request $request)
    {
        $path = substr($request->decodedPath(), strlen($request->segment(1)) + 1, -5);
        //$path = 'sensors/sensors_level/mortise_sensors';
        return $path;
    }*/

    /** Получает и взвращает дерево
     * @return array
     */
    public function getTree()
    {
        $tree = $this->_buildTree($this->category_id, $this->categoryParent);
        return $tree;
    }

    /** Строит дерево категорий
     * @param int $parent_id - id родительской категории
     * @param int $level - уровень вложенности
     * @return array
     */
    private function _buildTree($category_id = 0, $parent = false, $level = 0)
    {
        $tree       = [];
        $categories = $this->_getCategories($category_id, $parent);

        if ($categories)
        {
            if ($this->depth == 0 OR $this->depth > $level)
            {
                $level++;
                foreach ($categories as $category)
                {

                    $items = $this->_buildTree($category->id, true, $level);

                    $tree[$category->id] = [
                        'category'     => $category,
                        'id'           => $category->id,
                        'name'         => $category->name,
                        'uri'          => $category->uri,
                        'fulluri'      => $category->fulluri,
                        'image'        => $category->getImage(),
                        'content'      => $category->content,
                        'has_children' => ($items) ? true : false,
                        'children'     => $items,
                        'level'        => $level,
                        'open'         => false,
                    ];

                    if ($this->currentCategoryId)
                    {
                        $tree[$category->id]['open'] = $this->_isOpen($items);
                    }

                    if ($this->withProducts)
                    {
                        $products                                     = $this->_getProducts($category->id);
                        $tree[$category->id]['products']              = $products;
                        $tree[$category->id]['has_products']          = ($products) ? true : false;
                        $tree[$category->id]['children_has_products'] = $this->_hasProducts($items);
                    }

                    if ($this->withFiles)
                    {
                        $files                            = $category->getFiles();
                        $tree[$category->id]['files']     = $files;
                        $tree[$category->id]['has_files'] = ($files) ? true : false;
                    }
                }
            }
        }

        return $tree;
    }

    /** Определяет пометить ли категорию открытой (для показа в меню)
     * @param array $items
     * @return bool
     */
    private function _isOpen(array $items)
    {
        foreach ($items as $item)
        {
            if ($item['id'] == $this->currentCategoryId OR $item['open'] == true)
            {
                return true;
            }
        }

        return false;
    }


    private function _getCategories($category_id, $parent = false)
    {
        $category_field = 'id';
        if ($parent)
        {
            $category_field = 'parent_id';
        }

        return Category::where([$category_field => $category_id, 'status' => Category::STATUS_ACTIVE])->orderBy('sort')->get();
    }


    /** Получает продукты для данной категори
     * @param $category_id - id категории
     * @return array
     */
    private function _getProducts($category_id)
    {
        $products = [];
        $query    = Product::where(['category_id' => $category_id]);
        if ($this->productsLimit)
        {
            $query->limit($this->productsLimit);
        }

        $items = $query->get();
        if ($items)
        {
            foreach ($items as $item)
            {
                $products[$item->id] = [
                    'product'       => $item,
                    'id'            => $item->id,
                    'name'          => $item->name,
                    'uri'           => $item->uri,
                    'image'         => $item->getImage(),
                    'thumbnail'     => $item->getThumbnail(),
                    'short_content' => $item->short_content,
                    'content'       => $item->content,
                ];
            }
        }

        return $products;
    }

    /** Определяет есть ли продукты в категории
     * @param array $items - массив с продуктами
     * @return bool
     */
    private function _hasProducts(array $items)
    {
        foreach ($items as $item)
        {
            if (isset($item['products']) && count($item['products']) > 0)
            {
                return true;
            }
        }

        return false;
    }


}