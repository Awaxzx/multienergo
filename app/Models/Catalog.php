<?php

namespace App\Models;

Class Catalog
{
    public function getCategoriesTree($parent_id = 0, $withProducts = false, $totalProducts = 3)
    {
        $list       = [];
        $categories = Category::where(['parent_id' => $parent_id])->active()->get();
        if ($categories)
        {
            foreach ($categories as $category)
            {
                $list[$category->id] = [
                    'id'       => $category->id,
                    'name'     => $category->name,
                    'fulluri'  => $category->fulluri,
                    'content'  => $category->content,
                    'children' => $category->getCategoriesTree($category->id, $withProducts, $totalProducts),
                ];
            }
        }

    }
}