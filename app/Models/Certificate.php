<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Certificate extends Model
{
    use \App\Traits\ModelStatusTrait;
    use \App\Traits\ImageAndThumbTrait;

    const STATUS_ACTIVE   = 'active';
    const STATUS_INACTIVE = 'inactive';

    const IMAGE_DIRECTORY = 'certificates';
    const THUMB_DIRECTORY = self::IMAGE_DIRECTORY . '/thumbs';
    const THUMB_WIDTH     = 250;
    const THUMB_HEIGHT    = null;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable
        = [
            'name', 'image', 'sort', 'status',
        ];


}
