<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class File extends Model
{
    use \App\Traits\ModelStatusTrait;

    const STATUS_ACTIVE   = 'active';
    const STATUS_INACTIVE = 'inactive';

    const FILES_DIRECTORY = 'files';

    public function getFile()
    {
        return self::FILES_DIRECTORY . '/' . $this->name;
    }

    public function getSize()
    {
        if(\Storage::disk('public')->exists($this->getFile()))
        {
            $size  = \Storage::disk('public')->getSize($this->getFile());
            $units = ['b', 'Kb', 'Mb', 'Gb', 'Tb', 'Pb', 'Eb', 'Zb', 'Yb'];
            $power = $size > 0 ? floor(log($size, 1024)) : 0;

            return number_format($size / pow(1024, $power), 0, '.', ',') . ' ' . $units[$power];
        }

        return '0 b';
    }

    public function getFileType()
    {
        if(\Storage::disk('public')->exists($this->getFile()))
        {
            $size  = \Storage::disk('public')->getSize($this->getFile());
            $units = ['b', 'Kb', 'Mb', 'Gb', 'Tb', 'Pb', 'Eb', 'Zb', 'Yb'];
            $power = $size > 0 ? floor(log($size, 1024)) : 0;

            return number_format($size / pow(1024, $power), 0, '.', ',') . ' ' . $units[$power];
        }

        return '0 b';
    }

}
