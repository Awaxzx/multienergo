<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Page extends Model
{
    use \App\Traits\ModelStatusTrait;
    use \App\Traits\FlatTreeTrait;
    use \App\Traits\MetaTagsTrait;
    use \App\Traits\FilesTrait;

    const STATUS_ACTIVE   = 'active';
    const STATUS_INACTIVE = 'inactive';
    

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable
        = [
            'name', 'parent_id', 'uri', 'fulluri', 'content', 'sort', 'status',
        ];


    public function menus()
    {
        return $this->belongsToMany('App\Models\Menu', 'menu_pages');
    }


}
