<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class MenuPages extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
            'page_id', 'menu_id',
        ];
}
