<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Catalog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('categories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('parent_id');
            $table->string('name');
            $table->string('uri');
            $table->string('fulluri');
            $table->string('image')->nullable();
            $table->longText('content')->nullable();
            $table->timestamps();
            $table->integer('sort')->nullable();
            $table->string('status');
        });

        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('category_id')->references('id')->on('categories');
            $table->string('name');
            $table->string('uri');
            $table->string('image')->nullable();
            $table->text('short_content')->nullable();
            $table->longText('content')->nullable();
            $table->timestamps();
            $table->integer('sort')->nullable();
            $table->string('status');
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
        Schema::dropIfExists('categories');
    }
}
